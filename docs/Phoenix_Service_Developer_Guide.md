  ----------------------------------------
  Phoenix Service Developer's Guide
  
  **Distributed Platform (DP) Team**
  
  **02/05/2020**
  
  **Latest-Preview**
  ----------------------------------------
# 1. Overview - TCS GG7

Below are the versions referred in this document

-   PhoenixMsp Helm Template **3.3**

### 1.1 Introduction

This guide is for developers to develop and deploy Microservices and Web
applications to Phoenix Distribute Cloud Platform (other names:
Distributed Platform, DP, or Phoenix Platform).

Everyone should be able to go through the Quick Start section, but needs
some knowledge of Kubernetes and Microservice for the rest of sections.

In order to follow the examples or do any development/deployment work,
you have to have an OEC account (<http://openearth.community> ), join
the OEC project "DistServices" and have a Linux Dev VM in OEC (you can
submit an OEC ticket if you don't have one).

### 1.2 Contact and Support

If you want to learn more, or have questions, suggestions, or run into
trouble, here is the contact information:

Phoenix Platform (Distributed Platform) Email:
<DL_DistPlatform@halliburton.com>

Slack: distarch.slack.com

-   Join channel "**phoenix**" for Platform architecture, components and
    deployment related topics

-   Join channel "**java-msp**", "**node-msp**" for Microservice
    development related topics

-   Join channel "**support**" if you want to submit a support case

For fast response anytime anywhere, join our Slack channel!

### 1.3 Key Concepts

Before reading the rest of the developer guides, here are some key
concepts to be familiar with:

-   **Kubernetes** is the container orchestration system we are using.
    containers in kubernetes are called **pod**, and can contain one or
    more docker image. In most cases, you should have 1 docker image per
    pod. Note that we inject other images into the pod at deployment
    time (see next bullet): this injection system is called a
    **sidecar**. This is why you have to use the platform pod deployment
    scripts instead of doing it yourself from scratch.

-   **Istio** is a service mesh that is deployed across the cluster by
    injecting an **Envoy** service into each pod as a sidecar. Envoy
    intercepts all the http communication going in and out of the pod.
    This allows us to implement a variety of features across all
    microservices, without having to modify the implementation of each
    service. Authentication, authorization and tracing are all enabled
    by using Istio.

-   **Pods** are deployed in the cluster inside **namespaces.** A
    namespace is a way to more easily manage all the services deployed
    in the cluster. Listing all the services running inside a namespace
    will not list anything running in other namespaces. Teams are
    granted permission at the namespace level so they can't delete by
    accident a service running in a namespace of another team. The
    namespace should be named with the application it contains, i.e.
    tensorcloud, dwp, ali, etc...You can also have additional namespaces
    for dev, qa versions, i.e. ali-dev for developer work in progress,
    ali-qa for services that are ready to be tested/accepted, and ali
    for a stable or released version of the services. If your
    application depends on other services, like dsis for example, you
    shouldn't deploy them inside your namespace. Dsis containers will be
    running in the dsis namespace.

-   **ServiceAccounts** are tied to namespaces and grant application
    defined privileges to any pod running in that namespace. For
    example, one pod might need access to read/write access to s3
    buckets, while another pod might need read/write to s3 and access to
    EMR to run spark computations.

-   **Dpcli** (for **d**istributed**p**latform **CLI**) is a docker
    image with a lot of dev tools pre-installed to allow the developers
    to manage their namespace, i.e. deploy/delete/scale their pods. This
    is where you run most commands that interact with the cluster. Each
    developer doesn't have an aws or azure account. Dpcli helps with
    that by having extra permissions to do things on behalf of the
    platform administrator.

-   **HelmCharts** are a way to package your application for deployment
    on the cluster. Helmchart store in a yaml format a declaration of
    the various components that make up your application. If your
    application is made up of 7 pods, you will in general have 1
    helmchart that deploy all 7 together. Do not include the deployment
    of dependencies like dsis into your helmchart. Dsis will have its
    own helmchart. Note that it could make sense to package your
    application into several helmcharts if it will be sold in several
    pieces. For example, you could package your computation engine
    (batch api) separately from the front end UI and so would use 2 helm
    charts for that.

-   **IEnergyHub** is a landmark managed artifactory server available
    from anywhere on the internet as long as you have the correct access
    credentials. It is using the same technology as the artifactory in
    OEC (JFrog Artifactory) but the OEC artifactory is only accessible
    from within OEC and the HAL network, making it problematic to use to
    deploy in our AWS and Azure accounts.


### 2. Phoenix Architecture

#### 2.1 Overview

Phoenix is designed from ground up to be the Cloud Platform for hosting
varieties of Cloud native services and applications; it standardizes
security, deployment, and operational models for them. Phoenix Platform
is multi-cloud with cloud specific optimizations for better
manageability, stability, and SLAs. The following is a high level
architecture diagram specific to AWS deployment:

![](./media/image1.png)

#### 2.2 Technology Stack

The Phoenix Platform technology stack for M1 release on AWS includes:

-   AWS EKS (Managed Kubernetes Service)

-   AWS IAM (Identity and Access Management)

-   AWS EMR (Apache Spark)

-   AWS Aurora (Postgres database)

-   Istio (Service Mesh) in Kubernetes

-   Kubernetes Security (RBAC)

#### 2.3 Deployment Sandbox (Namespace)

Phoenix Platform is capable of providing security and isolation between
deployed services, thus allows each dev team to operate services only in
a sandbox (which is called a "namespace"). As the architecture diagram
above shows, system level namespaces are off-limit to dev teams. Each
application development team works in their own namespace (called team
namespace) which can be requested from Phoenix team. This is how
multi-tenancy for development works in Phoenix Platform.

Three namespaces will be created for a team (for example "afi") in a dev
cluster.

-   afi -- this is the namespace which will be available in a production
    cluster, where JWT authentication is enabled and enforced

-   afi-dev -- this is the dev namespace, no JWT authentication

-   afi-qa -- this is the qa or test namespace, no JWT authentication

There are no "dev" or "qa" namespaces in a production cluster.

#### 2.4 Service Account

Each dev team namespace contains a Kubernetes service account which
defines the kube API permissions in your deployed Microservices, and
contains Docker registry secrets to allow your pod to pull Docker images
from. You must deploy your Microservices with a service account.

## 3 Quick Start

### 3.1 Before You Start

The quick start makes these assumptions:

-   There is a functioning dev Phoenix cluster, we called it "distplat"
    in this guide

-   The main DNS is "distplat.landmarksoftware.io"

-   The admin DNS is "distplat-admin.landmarksoftware.io"

-   You have a OEC Linux dev machine

-   You team has been given a Platform namespace (a development
    Kubernetes namespace/sandbox) and a AWS user associated with this
    namespace. In this exercise we assume the namespace is "team-dev",
    the AWS user is "dp-team", and the aws profile is "myprofile" (See
    AWS CLI configuration option 3)

Here are some useful URIs in "distplat" cluster:

-   App Portal: portal.distplat.landmarksoftware.io

-   Dev Portal: dev.distplat.landmarksoftware.io

-   The URI for a deployed Microservice "fooservice":
    distplat.landmarksoftware.io/services/fooservice/

### 3.2 What to Deploy

You are going to deploy a Microservice and a Web application to the
Phoenix Platform. There is no dependency between these two exercises, so
you can start any of one first.

### 3.3 Get Deployment Tool

For security reason, you and your team are only allowed to deploy
services and applications in the team namespace. Be aware there is a
risk that you could override or delete a service or a web application
that was deployed by other team members, so create unique service
name/id.

#### 3.3.1 Platform Command Line Tool (dpcli)

Because of the high security consideration of the Phoenix Platform and
the complexity of setting up necessary tools to run a simple service
deployment, Platform team developed a command line tool "dpcli" which is
available as a Docker image and a helper shell script. Dpcli is
pre-installed and configured with these tools: python, pip, aws,
kubectl, helm, etc.

Be aware that dpcli image is **specific to a cluster**, so you can't use
the same dpcli image to work with different clusters. The name
convention for dpcli Docker image is:

<image repo uri\>/dpcli-<cluster name\>:<tag\>

**NOTE**: you need to run "docker login
distservices-docker.repo.openearth.io" with your OEC credentials for one
time, and if you see error message such as "Error response from daemon:
unauthorized: The client does not have permission for manifest", you
need to request for access by joining OEC project distservices via
<https://console.openearth.community/oec/browse/DistServices>

Run this command on your dev machine to pull the dpcli Docker image for
the cluster "distplat":
```
docker pull distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```
#### 3.3.2 dpcli Execution Mode

You can run dpcli Docker image interactively or run as a command.

**Interactive Mode**

Run dpcli interactively: (see next section for missing arguments)
```
docker run -it \--rm ......distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```
Type "exit" from shell prompt to exit the container.

**Command Mode**

Run dpcli as a command: (see next section for missing arguments)
```
docker run \--rm .....distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 kubectl get pods --n team-dev
```
### 3.4 Setup Development Environment 

You should already have obtained the AWS user access key id and secret
access key for the given Platform namespace.

The dpcli tool requires a proper AWS CLI configuration. There are three
AWS CLI configuration options you can apply, for detail please refer AWS
document
<https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html>

#### 3.4.1 Option 1: Pass Environment Variables

It's straightforward and doesn't require you to install AWS CLI, but not
very secure and convenience.

Start the dpcli Docker container in interactive mode:
```
docker run -it --rm -e AWS_ACCESS_KEY_ID=xxxx -e AWS_SECRET_ACCESS_KEY=xxxxx -e AWS_DEFAULT_REGION=us-east-1 distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```
Then inside the Docker container run (use your team namespace to replace
"team-dev"):
```
kubectl get pod -n team-dev
```
If you see error message such as "error: the server doesn\'t have a
resource type \"pod\"" or others, contact your cluster administrator --
the namespace is not set up correctly for you.

Note: Option 1 is not recommended.

#### 3.4.2 Option 2: Configure AWS CLI

If your dev machine has no AWC CLI installed, you need to install AWS
CLI first by following official AWS CLI installation guide
<https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html>

Then configure your AWS CLI:
```
$ aws configure
AWS Access Key ID [None]: xxxxxxx
AWS Secret Access Key [None]: xxxxxxxx
Default region name [None]: us-east-1
Default output format [None]: text
```
You will see ".aws" folder created in your home directory.

Start the dpcli Docker container in interactive mode:
```
docker run -it --rm -v $HOME/.aws:/root/.aws distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```
Perform the same verification steps as in option 1.

#### 3.4.3 Option 3: Configure Multiple Profiles

If you are working on multiple team namespaces, thus you have multiple
AWS user accounts, it's easier to set up profiles to switch between aws
users:
```
$ aws configure --profile user2
AWS Access Key ID [None]: xxxxxxxx
AWS Secret Access Key [None]: xxxxxxxx
Default region name [None]: us-east-1
Default output format [None]: text
```
Start the dpcli Docker container for a particular aws user profile
"myprofile" in interactive mode:
```
$ docker run -it --rm -v $HOME/.aws:/root/.aws -e AWS_PROFILE=myprofile distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```

Perform the same verification steps as in option 1.

Note: Option 3 is recommended and used in this Quick Start.

### 3.5 Publish Docker Image to iEnergy Hub

This iEnergy Hub Docker registry is accessible from your dev VM and
inside the Phoenix clusters:

**distplat-docker-milestone.hub.ienergycloud.io**

Steps to publish your docker images to this Docker registry on your dev
machine:

*  Login to the docker registry using credentials provided by Phoenix
    team:
```
docker login distplat-docker-milestone.hub.ienergycloud.io
```
*  Tag your image, for example:
```
docker tag distservices-docker.repo.openearth.io/distarch/fooservice:0.2 distplat-docker-milestone.hub.ienergycloud.io/distarch/fooservice:0.2
```

*  Push image to the registry:
```
docker push distplat-docker-milestone.hub.ienergycloud.io/distarch/fooservice:0.2
```

### 3.6 Deploy a Microservice


The goal of this exercise is to deploy a Microservice which you have
already built and dockerized.

#### 3.6.1 Namespaces

Microservices are deployed into a namespace

#### 3.6.2 Prepare Deployment

Microservice deployment template is available in the form of Helm Chart.
To follow this quick-start exercise you are not required to know Helm
Chart, but knowledge about Helm Chart is good to have.

git clone release-3.2 branch in the repo to get the template:
```
git clone -b release-3.2 <https://git.openearth.community/DistServices/helm-deploy.git>
cd helm-deploy
```
Chart phoenixmsp is the chart template we are going to use.

Edit deployment meta info in phoenixmsp/Chart.yaml for your
Microservice. If your Microservice name/id is "fooservice" and version
is 0.2.0, your Chart.yaml looks like this:
```
apiVersion: v1
appVersion: "0.2.0"
description: A Helm chart for Kubernetes
name: fooservice
version: 3.3
```
Next, edit phoenixmsp/values.yaml to provide cluster name, your Docker
image name and tag:
```
...
image:
 repository:distplat-docker-milestone.hub.ienergycloud.io/distarch/fooservice
 tag: '0.2.0'
 pullPolicy: Always
```
If your Microservice Docker container port is not 8080, you can change
it in
```
service:
 type: ClusterIP
 port: 80
 containerPort: 8081
```
Go to 'phoenixmsp/templates' and rename 'phoenix.yaml' to
'fooservice.yaml' (<Servicename\>.yaml)

#### 3.6.3 Execute Deployment

**Note**: in this exercise we utilize Helm Chart as a template engine to
generate Kubernetes yaml file and deploy it via "kubectl apply" command.
You should use "helm install" once you are familiar with Helm. Full Helm
capabilities will be discussed in "Helm Chart" section.

Before proceeding with helm install, here are the few details about global variables used in helm deployment.
Run the following command in the folder "helm-deploy" to generate
Kubernetes configure file "fooservice.yaml", which will deploy
fooservice to namespace "team-dev"

**HelmChart Global Variables**

**global.cluster.hosts**:  Cluster host name where to install the helm chart. <cluster_name>.landmarksoftware.io

**global.cloud.vendor:** Cloud vendor name (aws/azure). Default value – “aws”

**global.node.selector:** Preferred node type in a cluster to deploy application. (windows/linux). Note: As of now windows nodes available only in azure cluster. Default value-“linux”

**global.env.production:**  whether the helm chart deployment in production environment. (true/fasle)  Other than value production. Default value – “true”

**global.env:** This is parent element where we can add more global environment variables to this  if required along with ‘production’ env variable as above.

You can set in helm install command as _‘—set global.env.key1=value1’_
Refer [Global Variables](#8512-global-variables) section for more details
```
docker run --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=myprofile distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 helm template  myrelease --set global.cluster.hosts=<clustername>.landmarksoftware.io --namespace team-dev ./phoenixmsp --set global.env.production=false> fooservice.yaml
```
To deploy application in **azure** cluster provide the value for global variable ‘global.cloud.vendor’ as ‘azure’.
Default value is ‘aws’
```
docker run --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=myprofile  distservices-docker.repo.openearth.io/distarch/dpcli-distplat:1.0 **helm template myrelease --set** global.cluster.hosts=<clustername>.landmarksoftware.io --namespace team-dev ./phoenixmsp --set global.env.production=false **–set global.cloud.vendor=azure > fooservice.yaml**
```
To deploy the application in **windows** **nodes** provide the value for global variable ‘global.node.selecetor’ as ‘windows’. Default values is “linux”
```
docker run --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=myprofile  distservices-docker.repo.openearth.io/distarch/dpcli-distplat:1.0 **helm template myrelease --set** global.cluster.hosts=<clustername>.landmarksoftware.io --namespace team-dev ./phoenixmsp --set global.env.production=false –set global.cloud.vendor=azure **–set global.node.selector=windows > fooservice.yaml**
```
Currently windows nodes available only in azure clusters.

Note:

-   You must be in the folder "helm-deploy" to run this command

-   "helm template" is the command to generate the fooservice.yaml file

-   Make sure release name ("myrelease") is unique across
    all namespaces (imposed by Helm Chart)

-   You have to specify the namespace via "\--namespace" flag

-   Make sure the namespace is authorized by your AWS user account
    ("myprofile")

-   The namespace should contain a service account with Docker registry
    secret (done by Platform namespace provisioning)

-   It's up to you to run dpcli in interactive mode or command mode

-   You can create alias or simple shell script to wrapper this command

View the generated fooservice.yaml file to verify the content.

Run 'kubectl apply' command to deploy the service:
```
docker run --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=myprofile distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 kubectl apply -f fooservice.yaml --n team-dev
```

If successful, you'll see these outputs:
```
service/myrelease-fooservice created
deployment.apps/myrelease-fooservice created
virtualservice.networking.istio.io/myrelease-fooservice created
```
#### 3.6.4 Deployment Options

For different deployment options such as dev namespace or production
namespace development, please see "Helm Chart" section.

#### 3.6.5 Verify Deployment

You can access the deployed Microservice "fooservice" at
```
<https://distplat.landmarksoftware.io/services/myrelease-fooservice/>\...
```
#### 3.6.6 Troubleshoot Deployment

If your service is not accessible from the above service URI, follow
these step to diagnose:

*  Run dpcli in interactive mode
```
docker run -it --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=myprofile distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```

*  Make sure pod is running
```
[root@ad342d7c0032 app]# kubectl get pod -n team-dev
NAME                                  READY       STATUS      RESTARTS  AGE
dpbox-69b7dd5f97-5gj47                2/2         Running     0         19m
myrelease-fooservice-859fc74d45-lh7kf 2/2         Running     0         7m
tiller-deploy-6b8cb6b6d6-k5krq        1/1         Running     0         8h
```

If pod is not deployed correctly (status is not Running), you can find
the error messages by running
```
[root@ad342d7c0032 app]# kubectl describe pod myrelease-fooservice-859fc74d45-lh7kf --n team-dev
```
You can also view the Microservice docker container log:
```
[root@ad342d7c0032 app]# kubectl logs myrelease-fooservice-859fc74d45-lh7kf -c <container id\> -n team-dev
```
Or get a shell inside the Microservice docker container:
```
[root@ad342d7c0032 app]# kubectl exec myrelease-fooservice-859fc74d45-lh7kf -n team-dev -- /bin/bash
```

*  Make sure service is running

Next, use the dpbox pod deployed in the namespace to test if the service
internal DNS is working:
```
[root@ad342d7c0032 app]# kubectl exec dpbox-69b7dd5f97-5gj47 -n team-dev -c dpbox --curl -Is http://myrelease-fooservice/<some valid path>
```
If the service is not working, check the ports settings in Service and
Deployment sections in fooservice.yaml.

*  Make sure service proxy is configured correctly

Check the url path mappings in VirtualService section in
fooservice.yaml.

#### 3.6.7 Remove Deployment

To remove the deployment, use the same manifest file with kubectl delete
command. Make sure you are in the directory of the manifest file.
```
docker run --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=myprofile distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 kubectl delete -f fooservice.yaml --n team-dev
```

#### 3.6.8 Use Storage/PVC

To use Storage in your pod, first you need to create a PVC, and then bind PVC to the pod. All this happens inside templates. There are two types of storage block storage & file storage. Follow the steps below to create storage.

1  Create one or more block storage volumes by providing values to below fields in values.yaml of helm chart:

```
blockStorage:
  - storage: 5Mi
    mountpath: /tmp/blockstorage
  - storage: 1Mi
    mountpath: /tmp/otherblockstorage
```
Uncomment 'phoenix.blockstorageclaim' template in your phoenixmsp/templates/fooservice.yaml

2  Create one or more file storage volumes by providing values to below fields in values.yaml of helm chart:
     Added one file storage volume in the example, but helm template support multiple filestorage  volumes

 a. Dynamic filestorage volume  (creates a new folder in nfs server and not referring any existing path)

```
fileStorage:
  - storage: 5Mi
    mountpath: /tmp/filestorage
  #  name:
    type: dynamic
  #  nfspoint: /
  #  nfsserver:
```
       
   b. Static file storage volume (Refers the existing path in the nfs server)
 
```
fileStorage:
  - storage: 5Mi
    mountpath: /tmp/filestorage
 #   name:
    type: static
    nfspoint: <path in nfs-server> //Optional 
    nfsserver: <nfs-server-name> // Optional
```
 * Note: No PVC resource created in this case.

c. To refer an existing filestorage volume without creating a new one, In this case, type value will always be 'dynamic' because, we cannot create a pvc for 'static' type

```
fileStorage:
  - storage: 5Mi
    mountpath: /tmp/filestorage
    name: <external filstorgae- pvc name>
    type: dynamic
    nfspoint: <path in nfs-server> //Optional 
    nfsserver: <nfs-server-name> // Optional
```
 * Uncomment 'phoenix.filestorageclaim' template in your phoenixmsp/templates/fooservice.yaml
 
 *Note: In a single deployment, We can refer/create one or more filestorage claims as below.*
This example is creating one filestorage pvc and referring one filestorage pvc. To create  a filestorage pvc, we should always uncomment ‘filestorageclaim' template in phoenixmsp/templates/fooservice.yaml
```
fileStorage:
  - storage: 5Mi
    mountpath: /tmp/filestorage
 #   name: 
    type: static
    nfspoint: <path in nfs-server> //Optional 
    nfsserver: <nfs-server-name> // Optional
  - storage: 5Mi
    mountpath: /tmp/filestorage
    name: <external filstorgae- pvc name>
    type: dynamic
  #  nfspoint: /
  #  nfsserver:
```
Please refer section  [*Storage*](#_Storage)  for the detailed description.

#### 3.6.9 Use ConfigMap

To use ConfigMap in your deployment, first you need to create a
ConfigMap, and then bind ConfigMap to the pod. All this happens inside
templates. Follow the two steps below to create ConfigMap.

*  Create/Refer ConfigMaps by providing values to the below fields of
    values.yaml of helm chart:

*  Uncomment (to create)/comment (to refer) the 'configmapfromliteral' or 'configmapfromfile' template
    in phoenixmsp/templates/fooservice.yaml

**ConfigMap from Key Value pairs.**

1 To create configmap resources for deployment.
```
# To create configmap resource for deployment.
configmap:
  env:
    - data:
        HelloConfigMap: newvalue
    #  name:   
```
Uncomment the ‘configmapfromliteral’ in phoenixmsp/templates/fooservice.yaml

2 To refer configmap resources to the deployment 
```
# To create configmap resources for deployment.
configmap:
# provide data (key value pairs) to configmap, uncomment
'configmapfromliteral' template in your service.yaml,
 env:
   - name: <configmap-name-torefer>
   #  data: 
```
*  No need to uncomment the ‘configmapfromliteral’ in phoenixmsp/templates/fooservice.yaml as we are not going to create a new resource. 

*Note: In a single deployment, We can refer/create one or more configmaps as below.*
This example is referring one configmap and creating one configmap. To create  a configmap from literal, we should always uncomment ‘configmapfromliteral' template in phoenixmsp/templates/fooservice.yaml
```
# To create configmap resources for deployment.
configmap:
# provide data (key value pairs) to configmap, uncomment
'configmapfromliteral' template in your service.yaml,
 env:
   - name: <configmap-name-torefer>
   #  data:
   - data:
       HelloConfigMap: newvalue  
```


**ConfigMap from file**

1 To create and use a configmap
```
# Copy your configuration files to '/config' folder of this chart,
# uncomment 'configmapfromfile' template in your service.yaml and
# uncomment below lines, remove {} after file and should provide path and mouthpath values.
# Provide subpath if the file should not remove other files in the mountpath
configmap:
  file:
    - path: config/*
      mountpath: /javarun/config
      subpath: <filename>.json  // Optional
```
* Uncomment the 'configmapfromfile' template in phoenixmsp/templates/fooservice.yaml

2 To refer configmap resource to the deployment
```
# Copy your configuration files to '/config' folder of this chart,
# uncomment 'configmapfromfile' template in your service.yaml and
# uncomment below lines, remove {} after file and should provide path and mouthpath values.
# Provide subpath if the file should not remove other files in the mountpath
configmap:
  file:
    - name: <configmap name to refer>
 #     path: config/*
      mountpath: /javarun/config
      subpath: <filename>.json  // Optional
```
*  No need to uncomment the ‘configmapfromfile’ template in phoenixmsp/templates/fooservice.yaml as we are not going to create a new resource.

*Note: In a single deployment, We can refer/create one or more configmaps as below.*
This example is referring one configmap and creating one configmap. To create  a configmap from file, we should always uncomment ‘configmapfromfile' template in phoenixmsp/templates/fooservice.yaml
```
configmap:
  file:
    - name: <configmap name to refer>
 #     path: config/*
      mountpath: /javarun/config
      subpath: <filename>.json  // Optional
    - path: config/*
      mountpath: /javarun/config1
      subpath: <filename>.json  // Optional
```

**Please refer** [ConfigMap](#configmap)  for more detailed explanation.

#### 3.6.10 Secrets

To use Secrets in your deployment, you can create secrets, and then bind secrets to the pod (or) you can refer existing secrets. All this happens inside templates. Follow the two steps below to setup secrets.

 1.  Create/Refer Secrets by providing values to the below fields of values.yaml of helm chart

 2.   Uncomment/Comment the 'secretsfromliteral or 'secretsfromfile' template in 
    phoenixmsp/templates/fooservice.yaml

**Secrets from Key Value pairs**

1 To create secret resources for deployment

```
secret:
  env:
    - data
        USER_NAME: user
        PASSWORD: password
  #    name:
```

 Uncomment the ‘secretsfromliteral template in phoenixmsp/templates/fooservice.yaml

2 To refer existing secret to the deployment

```
secret:
  env:
    - name: <external-secret-name?
    #  data
```

* No need to uncomment the ‘secretsfromliteral template in phoenixmsp/templates/fooservice.yaml as we no need to create new secret

*Note: In a single deployment, We can refer/create one or more secrets as below.*
This example is referring one secret and creating one secret. To create  a secret  from literal, we should always uncomment ‘secretsfromliteral' template in phoenixmsp/templates/fooservice.yaml
```
secret:
  env:
    - name: <external-secret-name?
    #  data
    - data
        USER_NAME: user1
        PASSWORD: password1
    #  name:
```

**Secrets from file**

1 To create a secret 
```
secret:
  file:
    - path: secrets/*
      mountpath: /javarun/secret
  #    name:
```
 Uncomment the ‘secretsfromfile template in phoenixmsp/templates/fooservice.yaml

2  To refer a secret 
```
secret:
  file:
    - name: <external-secret-name>
 #     path: secrets/*
      mountpath: /javarun/secret
```
* No need to uncomment the ‘secretsfromliteral template in phoenixmsp/templates/fooservice.yaml as we no need to create new secret

*Note: In a single deployment, We can refer/create one or more secrets as below.*
This example is referring one secret and creating one secret. To create  a secret from file, we should always uncomment ‘secretsfromfile' template in phoenixmsp/templates/fooservice.yaml
```
secret:
  file:
    - name: <external-secret-name>
 #     path: secrets/*
      mountpath: /javarun/secret
    - path: secrets/*
      mountpath: /javarun/secret
  #    name:
`
```


#### 3.6.11 Assign Resources Requests & Limits

If a resource request is not met, the Platform will not deploy the pod.
The resource limit is the up limit that the Platform will give to a pod.

Note: If these values are not provided, default values could be assigned
based on your namespace settings. See section [Default Resource Limits
and Requests](#_Default_Resource_Limits).

To Assign cpu and memory requests and limits to a pod, uncomment the
following lines in values.ymal, adjust them as necessary, and remove the
curly braces after 'resources:'.
```
resources:
 requests:
  cpu: "200m" // 200 milli cpu which is equal to 0.2 cpu
  memory: "256Mi" // 256 MegaBytes.
 limits:
  cpu: "400m"
  memory: "512Mi"
```
#### 3.6.12 Readiness and Liveness Probes

Kubernetes uses liveness probes to know when to restart a Container. For
example, liveness probes could catch a deadlock, where an application is
running, but unable to make progress. Restarting a Container in such a
state can help to make the application more available despite bugs.

Kubernetes uses readiness probes to know when a Container is ready to
start accepting traffic. A Pod is considered ready when all of its
Containers are ready. One use of this signal is to control which Pods
are used as backends for Services. When a Pod is not ready, it is
removed from Service load balancers.

More information about readiness and liveness probes may be found
[here](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/).

There are three types of readiness and liveness probes available in
Kubernetes:
[tcp](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#define-a-tcp-liveness-probe),
[command](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#define-a-liveness-command)
and
[http](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#define-a-liveness-http-request)
probes. For instance, http probes shown below:
```
livenessProbe:
 httpGet:
  path: /msp/home/health
  port: 8080
 initialDelaySeconds: 10
  readinessProbe:
 httpGet:
  path: /msp/home/readiness
  port: 8080
 initialDelaySeconds: 10
```
You may configure liveness and readiness probes using command from
[here](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#configure-probes).

### 3.7 JMX Monitoring for Java Apps

To enable JMX in your microservice, you need to set these JAVA options
in the deployment yaml file and then redeploy your microservice.
```
-Dcom.sun.management.jmxremote.port=9010 \
-Dcom.sun.management.jmxremote.rmi.port=9010 \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false \
-Djava.rmi.server.hostname=127.0.0.1 \
```

You can make JMX connection (launch jconsole) to the deployed
Microservice within the dpcli container or your dev VM.

#### 3.7.1 Make JXM Connection in dpcli Container

Steps to launch jconsole in dpcli container:

*  Run the following command in dev vm to enable X11.
```
xhost +
```
*  Start dpcli container in interactive mode
```
docker run -it --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=team-dev -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```

*  Run kubectl inside the dpcli container to get the pod
```
[root@ad342d7c0032 app]# kubectl get pods -n <namespace>
NAME                        READY     STATUS     RESTARTS     AGE
fooservice-6b4c996865-5wwtp 2/2       Running    0            1d
```
*  Run port-forward to the pod
Note: Also should not reconnect to dpcli container from new terminal.
That will cause an error. Instead use **nohup** command to continue with
next command without stopping executing command
```
[root\@ad342d7c0032 app]# nohup kubectl port-forward fooservice-95dc4fd49-pfsgt 9010 -n team-dev
```
Note: In case if you were not able to stop the port listening, use the
below command to kill the port listening
```
lsof -n -i4TCP:[PORT] | grep LISTEN | awk '{ print $2 }' | xargs kill
```
*  Launch JConsole inside the dpcli container
```
[root@ad342d7c0032 app]# jconsole 127.0.0.1:9010
```
![](./media/image2.png)

#### 3.7.2 Make JMX Connection in Dev VM 

First you need to get dpcli image and then setup kubectl in your dev vm
by following these steps:

**Note:** The files in the .aws folder created in your dev machine has
necessary permissions. ```drwxr-xr-x```

1  In an empty folder of your dev vm, start dpcli container in
    interactive mode
```
docker run -it --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=<namespace> distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```
2  Inside the dpcli container, copy these files from dpcli container to
   your dev VM
```
[root@ad342d7c0032 app]# cp /root/.kube/config .
[root@ad342d7c0032 app]# cp /usr/local/bin/kubectl .
```

3  Exit from dpcli container and move files in your dev VM
```
sudo mkdir /.kube
sudo cp config /.kube
sudo cp kubectl /usr/local/bin
```

4  Set kubeconfig env variable
```
export KUBECONFIG=/.kube/config
```
5  Rename the profile name of required profile in credentials file of
    .aws folder to 'default' temporarily.
```
[default]
aws_access_key_id: XXXXXXXXXXXXXX
aws_secret_access_key: XXXXXXXXXX
```
6 Now, you should be able to use kubectl to find the pod you want to
    connect to
```
kubectl get pods -n team-dev
NAME                       READY    STATUS     RESTARTS   AGE
fooservice-95dc4fd49-pfsgt 2/2      Running    0          31m
```
7  JMX port forward to local port
```
kubectl port-forward fooservice-95dc4fd49-pfsgt 9010 -n team-dev
```
Note: In case if you are not able to stop the port listening, use the
below command to kill the port listening
```
lsof -n -i4TCP:[PORT] | grep LISTEN | awk '{ print $2 }' | xargs kill
```
8  Launch jconsole to the forwarded local JMX port

    Open the new terminal and execute the below command
```
jconsole 127.0.0.1:9010
```

### 3.8 Remote Debugging for java apps

#### 3.8.1 From DEV VM

To start remote debugging from DEV VM finish the kubernetes setup in
local DEV VM. Refer steps 1 to 6 of
[section](#372-make-jmx-connection-in-dev-vm)

1.  You can specify environment variables in deployment and Service
    sections in .yaml file for remote debug host and port.

Deployment:

* Add the below env variable
```
env:
 -name: MSDP_JAVA_OPTIONS
  value: "-Xmx1024m -Xdebug -agentlib:jdwp=transport=dt_socket,address=9999,server=y,suspend=n"
```
*  Add the below target port
```
Spec:
 Containers:
  ports:
   - name: debug
     containerPort: 9999
```
Service:
```
 - port: 9999
   targetPort: 9999
   protocol: TCP
   name: debug
```
*  Port forward to local port
```
kubectl port-forward <POD_NAME> 9999:9999 --n <namespace>
```
*  Keep debug points in your eclipse workspace.

*  Specify debug configuration in Eclipse

> ![image](./media/image3.png)

*  Open the required service api page and hit any endpoint and debug
    should start in eclipse

    ![](./media/image4.png)
    
#### 3.8.2 From dpcli Container

*  Execute the below command to connect to dpcli image. In this, we are
    mapping required project, .m2 library, X11 server mapping for
    graphics. This includes eclipse install with which we will continue
    remote debugging
```
docker run -it --rm -v $HOME/.aws:/root/.aws -v HOME/.m2:/root/.m2 -v $HOME/<Project_path>:/root/app/<project_folder> -v $(pwd):/root/app -e AWS_PROFILE=team-dev -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```

*  Launch eclipse. This command will make sure eclipse will still exit
    even you exit from eclipse command
```
nohup eclipse &
```
*  Set up your workspace and set debug points.

*  Port forward to local port
```
kubectl port-forward <POD_NAME> 9999:9999 --n <namespace>
```

*  Specify debug configuration in Eclipse:

> ![image](./media/image3.png)

*  Open the required service api page and hit any endpoint and debug
    should start in
    eclipse![](./media/image4.png)
    
## 4 Deploy a Web Framework (WF) Application

### 4.1 Create WF SP Application using Getting Started SPA

In OEC Environment, create an SPA using seed Getting Started which uses
remote hosted web framework services.

**Step 1**: Clone the getting-started seed
```
$ git clone --depth 1 --branch=v3.0 https://git.openearth.community/Getting-Started/webframework-examples/getting-started.git
```
**Step 2:** Generate '*.npmrc'* file for Jfrog repository

Login to Jfrog artifactory in OEC
https://artifacts.repo.openearth.community/artifactory

Click on the artifacts icon on the left navigation menu and click Set Me
Up icon on the right corner as shown below.

![](./media/image5.png)

In the popup which opens configure the following and click on the arrow
next to the OEC credentials box.

![](./media/image6.png)

Create a file in your user folder with name '.npmrc' and copy the
contents of section: **Using basic authentication** in the same popup.

![](./media/image7.png)

Save this file and run the following command in a terminal window.
```
$ npm config set registry
<https://repo.openearth.community/artifactory/api/npm/webframework-npm-all/>
```
on execution of the above command, your .npmrc file will look like
below:
```
auth=cHJhYmF.... hwZWND
email=..........@halliburton.com
always-auth=true
registry=https://repo.openearth.community/artifactory/api/npm/webframework-npm-all/
```
you can also validate by running below command:
```
$ npm config list
```
**Step 3:**

Register a keycloak client for the application. Refer [Register Keycloak
Client](#_Register_a_keycloak)

Copy the **keycloak.json** to **getting-started/apps/dashboard/src**

**Step 4:**

In getting-started folder, run the below commands
```
$ npm install
$ npm run build
```

**Step 5:**

To run the application locally using services hosted remotely, follow
the below steps.

Open 'proxy.conf.json' in the application root folder and update the
file with required remote service URLs as below:
```
{
 "/searchservice/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/applications/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/currentuser/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/appmenuConfig/**": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/datatransferservice/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/accessManagement/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/sc/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/logViewer/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/logData/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/js/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/resource/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/lea/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/serviceregistry/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 },
 "/dataqueryservice/*": {
 "target": "https://<cluster>.<dns>/services/routerservice/",
 "secure": false,
 "logLevel": "debug",
 "changeOrigin": true
 }
}
```
**Step 6:**

Run below command to start the application:
```
$ npm start
```
Access your application using <http://localhost:4200/>

Now to import the configurations for your application, edit '.wfrc' file
under configs folder.

The keycloak resource value should be same as the 'client\_id'
registered for this application.
```
{
 "dsisConfigUrl": "https://conf.<cluster>.<dns>/dsis-config/api/v1/applications",
 "keycloakConfig": {
 "realm": "DecisionSpace_Integration_Server",
 "auth-server-url": "https://<cluster>.<dns>/ /auth",
 "ssl-required": "none",
 "resource": "myapp",
 "public-client": true,
 "confidential-port": 0
},
 "serviceRegistryConfigs": [ {…},],
 "deleteApplication": false,
 "launchTechnologyConfigs": {…},
 "emailServiceConfig": {…},
 "supportAssistantConfig": {…},
 "importPath": "./data",
 "exportPath": “. /data/export”
}
```
After making the changes, from configs folder execute the commands in
steps: 3 through 7 under section [Configuration using the Tar
Ball](#_Configuration_using_the)**.**

For more details refer:
<http://wiki.openearth.community/wikidocs/Installation.pdf>

### 4.2 Steps to Automate the deployment of the application

The below steps will help you to automate the following:

-   **On deploying the application,**

    -   Registering a keycloak client for the application.

    -   Adding a redirect url for the app to the client registered.

    -   Creating application template and import the required
        configurations to dsis-config service.

-   **On deleting the application,**

    -   Delete the keycloak client.

    -   Delete the application configurations from dsis-config service.

#### 4.2.1 Add deployment script to source code

Add a file inside \`configs\` directory of the application called
\`deploy.sh\` with below sample content.

***deploy.sh***
```
hook=$1
hosts=$2
appname=$3
chartname=$4
realm=$5
adminuname=$WF_ADMIN_USER
adminpwd=$WF_ADMIN_PASSWORD
case "$hook" in
'pre-install')
echo "Running Pre-install script "
if [ -z "${FRESH_INSTALL}" ] || [ "${FRESH_INSTALL}" = "true" ]
then
 echo "[Auto Deploy] : FRESH INSTALL"
 . ./node_modules/@webframework/scripts/deployment/auto-import-scripts.sh $hosts
$adminuname $adminpwd $appname $chartname $realm
else
 echo "[Auto Deploy] : \$FRESH_INSTALL=$FRESH_INSTALL"
 echo "[Auto Deploy] : SKIPPING IMPORT !!"
fi
;;
'post-install')
echo "Running Post-Install script "
;;
'pre-upgrade')
echo "Running Pre-Upgrade script "
# Invoke your script here #
;;
'post-upgrade')
echo "Running Post-Upgrade script "
# Invoke your script here #
;;
'post-delete')
echo "Running Post-Delete script "
# Invoke your script here #
. ./node_modules/@webframework/scripts/deployment/auto-delete-scripts.sh $hosts
$adminuname $adminpwd $appname $chartname $realm
;;
esac
exit 0
```
#### 4.2.2 Update webpack config for scripts

Update webpack.config.js file to include deployment script in the build.

***webpack.config.js***
```
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = {
 target: "node",
 mode: "production",
 entry: {
 app: ["./scripts-exec.js"]
 },
 output: {
 path: path.resolve(__dirname, "./dist"),
 filename: "scripts-exec.js"
 },
 plugins: [
 new CopyWebpackPlugin([
 {
 from: __dirname + '/data/**'
 },
 {
 from: __dirname + '/.wfrc'
 },
 {
 from: __dirname + '/deploy.sh'
 },
 {
 from: __dirname + '/node_modules/@webframework/scripts/deployment/',
 to: 'node_modules/@webframework/scripts/deployment/'
 }
 ])]
};
```
#### 4.2.3 Update webframework utility version

The basic scripts are provided by webframework. These scripts were added
in npm package \@webframework/scripts\@4.3.5.

Make sure the version you are using is 4.3.5 or above. Modify this in
config/package.json file

You can run the below command under configs folder to update the version
in your dependency tree.
```
[configs]$ npm update @webframework/scripts
```
#### 4.2.4 Creating Docker image for Scripts and configurations.

Create the docker build script (dockerfile) for your configs directory.

This can contain any external db scripts for creating table schemas, any
pre/post-install/upgrade/post-delete scripts required for setting up
your application. Add commands to install any tools that you may need
for running these scripts.

***Scripts.Dockerfile***
```
FROM node:8
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY configs/dist/ ./
ENTRYPOINT [ "bash","deploy.sh"]
```
#### 4.2.5 Update Docker publisher script under tools folder

Update docker-publisher.sh file under tools folder with below sample
content. This file is responsible for publishing the images to the
repository.
```
username=$ARTIFACTORY_USER
password=$ARTIFACTORY_PASSWORD
repo=$DOCKER_REGISTRY
ienergy_username=$CI_IENERGY_USER
ienergy_password=$CI_IENERGY_PASSWORD
ienergy_repo=$IENERGY_DOCKER_REGISTRY
scripts_docker_file=Scripts.Dockerfile
appName=$(node -pe 'JSON.parse(process.argv[1]).name' "$(cat package.json)")
version=$(node -pe 'JSON.parse(process.argv[1]).version' "$(cat package.json)")
serviceVersion=$(echo $version|awk -F '-' '{print $1}')
commitSHA=${GIT_COMMIT}
commit=${commitSHA:0:8}
releaseTag=${appName}:${serviceVersion}
canaryTag=${appName}:${serviceVersion}
scriptsCanaryTag=${appName}-scripts:${serviceVersion}
scriptsReleaseTag=${appName}-scripts:${serviceVersion}
echo $repo $releaseTag $canaryTag
echo '********Publish to Webframework Registry*******'
docker login -u ${username} -p ${password} ${repo}
docker login -u ${ienergy_username} -p ${ienergy_password} ${ienergy_repo}
echo '********Building Docker Image*********'
docker build -t $canaryTag .
echo '********Building Docker Image For Scripts*********'
docker build -f $scripts_docker_file -t $scriptsCanaryTag .
echo '******* BUILD COMPLETED *********'
if [ $BRANCH_NAME == 'master' ]; then
echo '********* Branch Name = master*******'
echo '******Creating tags for webframework*******'
docker tag ${canaryTag} ${repo}/$releaseTag
echo '******Deliver to webframework registry*******'
docker push ${repo}/$releaseTag
docker rmi ${repo}/${releaseTag}
docker tag $scriptsCanaryTag $repo/$scriptsReleaseTag
docker push $repo/$scriptsReleaseTag
docker rmi $repo/$scriptsReleaseTag
echo '******Creating tags for IEnergy Hub********'
docker tag ${canaryTag} ${ienergy_repo}/distarch/wf/staging/$releaseTag
echo '******Deliver to IEnergy Hub*******'
docker push ${ienergy_repo}/distarch/wf/staging/$releaseTag
docker rmi ${ienergy_repo}/distarch/wf/staging/$releaseTag
docker tag $scriptsCanaryTag ${ienergy_repo}/distarch/wf/staging/$scriptsReleaseTag
docker push ${ienergy_repo}/distarch/wf/staging/$scriptsReleaseTag
docker rmi ${ienergy_repo}/distarch/wf/staging/$scriptsReleaseTag
else
echo '******Canary*******'
echo '******Creating tags for webframework*******'
docker tag ${canaryTag} ${repo}/${canaryTag}
echo '******Deliver to webframework registry*******'
docker push ${repo}/${canaryTag}
docker rmi ${repo}/${canaryTag}
docker tag $scriptsCanaryTag $repo/$scriptsCanaryTag
docker push $repo/$scriptsCanaryTag
docker rmi $repo/$scriptsCanaryTag
echo '******Creating tags for IEnergy Hub********'
docker tag ${canaryTag} ${ienergy_repo}/distarch/wf/development/$canaryTag
echo '******Deliver to IEnergy Hub*******'
docker push ${ienergy_repo}/distarch/wf/development/$canaryTag
docker rmi ${ienergy_repo}/distarch/wf/development/$canaryTag
docker tag $scriptsCanaryTag ${ienergy_repo}/distarch/wf/development/$scriptsCanaryTag
docker push ${ienergy_repo}/distarch/wf/development/$scriptsCanaryTag
docker rmi ${ienergy_repo}/distarch/wf/development/$scriptsCanaryTag
fi
docker rmi $canaryTag
docker rmi $scriptsCanaryTag
```
#### 4.2.6 Ensure the ". wfrc" file structure

Ensure "*. wfrc"* file inside configs folder is in the below template
structure
```
{
 "dsisConfigUrl": "<hostname>:<port>/dsis-config/api/v1/applications",
 "keycloakConfig": {<Content_of_Keycloak.json of SPA Client>},
 "deleteApplication": true,
 "importPath": “./data",
 "exportPath": "./data/export"
}
```
#### 4.2.7 Bundle the distribution for scripts with above changes

From within the configs folder, run the following command
```
[configs]$ npm run bundle
```

### 4.3 Build WF Artifacts

After you create your WF application, you can run
tools/docker-publisher.sh in the project to build, Dockerize and publish
the artifact.

The execution of the script will

-   Build the application (Generate production files in 'dist' folder)

-   Dockerize the application (Docker file is available). It will create
    two images.

    -   myapp -- Image of the application

    -   myapp-scripts -- Image for the configurations

-   Publish the artifact to iEnergy Hub repository

    -   <ienergy_repo>/distarch/wf/development/myapp

    -   <ienergy_repo>/distarch/wf/development/myapp-scripts

Provide the following necessary input parameters to execute the
docker-publisher.sh file.
```
username=$ARTIFACTORY_USER
password=$ARTIFACTORY_PASSWORD
repo=$DOCKER_REGISTRY
ienergy_username=$CI_IENERGY_USER
ienergy_password=$CI_IENERGY_PASSWORD
ienergy_repo=$IENERGY_DOCKER_REGISTRY
```
User can set up CI Pipeline for the application following
<http://wiki.openearth.community/setting-up-jenkins-pipelines>

### 4.4 Prepare Deployment

git clone this repository:
```
git clone -b release-3.2 <https://git.openearth.community/DistServices/helm-deploy.git>
cd helm-deploy
```
helm-deploy folder will have the below structure:

![](./media/image8.png)

After you clone the helm-deploy project, copy and modify phoenixmsp
folder as the chart template to create helm chart for your web
application.

-   Modify the phoenixmsp folder name as your chart name.

    -   Example: myapp

-   Rename phoenix.yaml in templates folder to <APP_NAME>.yaml

    -   Example: myapp.yaml

Edit deployment metadata information in Chart.yaml.

If your application name is myapp and version is 1.0, your Chart.yaml
looks like this:
```
apiVersion: v1
appVersion: "1.0"
description: A Helm chart for myapp SPA
name: myapp
version: 3.3
```
\*name should be same as the folder name

appVersion can be the version of the application used

Next, edit myapp/values.yaml to manage the following values:

-   Update the global variables
    -   hosts
    -   production -- true/false
    -   vendor
        -   aws
        -   azure
-   provide 'app_type' value as "web-app"
    -   app_type: "web-app"
-   repository name -- repository path of the docker image for the web
    application
-   tag value -- tag version of the docker image
-   containerPort
```
global: 
  cluster: 
    hosts: 
  env: 
    production: true 
  cloud: 
    vendor: aws

app_type: "web-app" 
replicaCount: 1 

image: 
  repository: <repo-milestone>.hub.ienergycloud.io/distarch/wf/development/myapp 
  tag: 1.0.1 
  pullPolicy: Always

--- 
service: 
  type: ClusterIP 
ports: 
  - port: 80 
    containerPort: 80 
    name: http 
    protocol: TCP
```


**Important Note:**

The application name (myapp) is used to create the DNS name which must
be a DNS-1035 label.

A DNS-1035 label must consist of lower case alphanumeric characters or
'-', start with an alphabetic character, and end with an alphanumeric
character (e.g. 'testapp', 'my-name', or 'abc-123', regex used for
validation is a-z]([-a-z0-9]\*[a-z0-9])?')

#### 4.4.1 Readiness and Liveness Probe

-   If there is a health endpoint, mention it in the path variable under
    httpGet. Use / if there is no specific health endpoint available.

-   If liveness and readiness is not necessary, comment lines under
    livenessProbe and readinessProbe and add {} after 'livenessProbe:
    {}' and 'readinessProbe: {}'

**livenessProbe:**
```
livenessProbe: 
  httpGet: 
    path: / 
    port: 80 
    scheme: HTTP 
  initialDelaySeconds: 10 
readinessProbe: 
  httpGet: 
    path: / 
    port: 80 
    scheme: HTTP 
    initialDelaySeconds: 10
```
#### 4.4.2 Adding as a Product to iEnergy Console

If the web application needs to be registered as a product to the
iEnergy Console application for the admin user's dashboard, following
annotations value should be supplied as below.
Supply annotations for web application in values.yaml under webapp
annotations

*   product_type: Type of the application
    *   Possible options are 1) "0" for Web 2) "1" for 2D 3) "3" for 3D
    *   The product_type value if not supplied, the web application
        will not be registered to the Web framework iEnergy console.
*   product_category: Product Category is used to filter the
    applications based on the values provided
    *   Example: "Dashboards"
    *   *It can be a single value or a list of values separated by
        comma. Example: "Geology, Dashboard"*
    *   The values provided here should be already present as a Domain
        in iEnergy Console application
*   product_description: A description of the application
*   product_display_name: Name of the application which gets displayed
    in the dashboard.
*   product_icon: Path of the icon in the container.
    *   Example: if the file is located under html/assets/favicon.ico,
        then the value of product_icon will be "assets/favicon.ico"
*   registerToAllUsers: To add the product to all the users or only the
    Admin user of IEnergy admin console
    *   Possible options are 1) true 2) false
    *   Default value is false
```
webapp: 
  annotations: 
    product_category: "Dashboards" 
    product_description: "myapp description" 
    product_display_name: "My App" 
    product_icon: "dev.png" 
    product_type: "0" 
    registerToAllUsers: false
```

#### 4.4.3 Keycloak Configuration

The web application must authenticate with a keycloak server using a JWT
Token with the following information:

-   ClientID -- Registered for your web application. Provide this value
    for the resource variable as below.

-   Realm -- Under which the client is created

Refer [Register client](#_Register_a_keycloak) section for more details
on registering a client for the web application

In Values.yaml, under section webapp -\> keycloak, values should be
provided
```
webapp: 
  keycloak: 
    realm: "DecisionSpace_Integration_Server" 
    resource: "dev-portal" 
    mountpath: /usr/share/nginx/html/keycloak.json 
    subpath: keycloak.json
```
mountpath: is the path of the nginx server folder to place the file from
config map

subpath: without this property, all the existing files inside
**usr/share/nginx/html** folder will be removed and overwritten by the
keycloak.json file. This should be the name of the file expected in the
web application.

To include keycloak authentication in the application, Keycloak comes
with a client-side JavaScript library keycloak.js

For usage, Refer:
<https://www.keycloak.org/docs/2.5/securing_apps/topics/oidc/javascript-adapter.html>

For more details on config map, refer [Config Map](#configmap)

#### 4.4.4 Service Routes

If additional routes are required for accessing the backend services, it
must be added to the virtual service.

In values.yaml file remove the {} under routes and provide the values as
below sample:
```
routes: 
  serviceregistry: 
    uri: /serviceregistry 
    host: routerservice.wf-system.svc.cluster.local 
  applications: 
    uri: /applications 
    host: routerservice.wf-system.svc.cluster.local 
  helloservice: 
    uri: /services/helloservice 
    host: helloservice.default.svc.cluster.local 
  platadmin: 
    prefix: /services/platadmin/ 
    uri: / 
    host: platadmin.plat-system.svc.cluster.local
```
**Warning:**

Providing absolute URL of the endpoint from you web application results
in the CORS issue, since the proxy is not enabled with CORS. So Always
provide relative path of the endpoints.

If the prefix is different from uri value, each can be supplied
separately as provided in the example above.

Any endpoints deployed in prod namespace should be accessed providing
authentication

#### 4.4.5 Helm Hooks

A section 'hooks' for adding Helm hooks for our service is added.

To Enable helm hooks, provide the schema\_image details.

Incase if you want to Override job parameters like hook-delete-policy
and restartPolicy uncomment these under respective jobs

*  **hook-delete-policy:** when to delete the jobs created.
    Possible values are
    *  hook-succeeded
    *  before-hook-creation
    *  hook-failed.
    Default is \'hook-succeeded\'
*  **restartPolicy:** 1) Never 2) OnFailure
*  To use the environment variables for the hooks,
    *  For webframework applications,
        *  Mandate enable WF_ADMIN_USER and WF_ADMIN_PASSWORD and
           ensure wfadminapp-secret is available in the namespace the
           app is deployed
        * FRESH_INSTALL: During reinstallation, to decide if the
          configurations needs to be updated or not. Options are 1)
          true 2) false.
    Default value is true.

Currently, Hooks schema image are available only for the webframework
web applications.
```
hooks: 
  schema_image: 
    repository: distplat-docker-milestone.hub.ienergycloud.io/distarch/myapp-scripts 
    tag: 1.0 
    pullPolicy: Always 

  preInstall: 
    hookDeletePolicy: hook-succeeded 
    restartPolicy: Never 
    arguments: [distteam.landmarksoftware.io, myapp, myapp, Ds_Integration_Server] 

  postInstall: {} 
    hookDeletePolicy: hook-succeeded 
    restartPolicy: Never 
    arguments: [arg1, arg2...] 

  postDelete: {} 
    hookDeletePolicy: hook-succeeded 
    restartPolicy: Never 
    arguments: [arg1, arg2...] 
  env: 
    - name: WF_ADMIN_USER 
      valueFrom: 
        secretKeyRef: 
            name: wfadminapp-secret 
            key: wfusername 
    - name: WF_ADMIN_PASSWORD 
      valueFrom: 
        secretKeyRef: 
            name: wfadminapp-secret 
            key: wfpassword 
    - name: FRESH_INSTALL 
      value: "true"
```

### 4.5 Verify Helm Chart

To execute helm commands, first step is to connect to dpcli image from
helm-deploy folder.

Use helm lint command to examine chart for possible issues like template error,yaml syntax error, kubernetes manifest syntax error etc. 
```
helm lint <chart-folder-name>
```
To validate the resulting manifest file, execute the following helm
command. This creates a manifest file which is used to deploy the
application
```
[user@i-X helm-deploy] $ helm template <release-name> ./<chart-folder-name> --namespace <namespace> --set global.env.production=false > <filename>.yaml 

Example:
[user@i-X helm-deploy] $ helm template myapp ./myapp --namespace team-dev --set global.env.production=false > myapp.yaml
```
Validate the file myapp.yaml created in the folder for correctness.

Use below command sample to validate manifest without creating a file.
```
[user@i-X helm-deploy] $ helm install myapp --debug --dry-run ./myapp --namespace dp-dev --set global.env.production=false
```

### 4.6 Package and Publish Helm chart

Refer section [Packaging and
Publishing](#rhere-arepackage-and-publish-helm-chart) for the steps.

To understand installing helm chart for production vs non-prod
environment refer: [Naming Conventions for
Services](#_Naming_Convensions_For)

### 4.7 Execute Deployment

If the manifest file generated using the helm chart is correct, refer
[Deploy Helm Chart](#deploy-helm-chart) section for the steps.

To understand installing helm chart for production vs non-prod
environment refer: [Naming Conventions for
Services](#_Naming_Convensions_For)

### 4.8 Access Application in Browser

**Scenario 1: (Deployed in Production name space)**
The application can be accessed from browser using
https://<chartname\>.<hosts\>/
URL to access the application:
-   aws - <https://myapp.distplat.landmarksoftware.io/>
-   azure - <https://myapp.distdwp.azure.landmarksoftware.io/>

**Scenario 2: (Deployed in dev namespace)**
The application can be accessed from browser using
https://<releasename\>-<chartname\>.<hosts\>/
URL to access the application:
-   aws - <https://myrelease-myapp.distplat.landmarksoftware.io/>
-   azure - <https://myrelease-myapp.distdwp.azure.landmarksoftware.io/>

### 4.9 Configuration using the Tar Ball

The last step is to import the application configuration:

This is required only in case of manual process, otherwise, registering
keycloak client and importing configurations were handled by helm hooks,
an automatic process happens with helm deployment.

```**\*** within dpcli for your cluster execute the below steps```

**Step 1:** Download the tar ball which has the utility and the data to
be imported to the config database.
```
$ jfrog rt dl distplat-maven-milestone/<TARGET_FILE_PATH> 
Example: 
$ jfrog rt dl distplat-maven-milestone/myapp-0.1.tar
```
**Step 2:** Unzip the tar ball into a folder.

\*Ensure the folder myapp is created and has write access before copying
the contents of the tar file.
```
$ tar -C myapp -xvf myapp-0.1.tar
```
**Step 3:** Update the ".wfrc" file inside the myapp folder with
required changes if any.

This file might be a hidden file. Modify dsisConfigUrl and
KeycloakConfig configurations

**Step 4:** Run below command to create a key required for
encryption/decryption of texts.
```
[...Myapp] $ node scripts-exec.js create-crypto-key
```
The above command results in:
```
Invoking command: create-crypto-key
Reading Configuration File From /home/…/gettingstated_tar/myapp/.wfrc
Crypto Key: ab834a057a333f94c9e581cadf28e7d2
```
**Step 5:** Create environment variable called CRYPTO\_KEY with values
obtained in the above step.
```
$ export CRYPTO_KEY=ab834a057a333f94c9e581cadf28e7d2
```
**Step 6:** Encrypt credentials using below command
```
$ node scripts-exec.js encrypt –-p mypassword
```
The above command will provide the encrypted text of your password

![](./media/image9.png)


**Step 7:** Create two environment variables with keys KEYCLOAK\_USER &&
KEYCLOAK\_PASSWORD with values as username and password of a user in
keycloak service.

User should have READ/WRITE permissions for dsis config plugin.
```
$ export KEYCLOAK_USER=myusername 
$ export KEYCLOAK_PASSWORD=XzgiHcS42fOuL6ePFOd3Q==
```
**Step 8:** Update the ".wfrc" file inside the myapp folder with
required changes if any.

Once the .wfrc file is updated execute the below command. This file
might be a hidden file.
```
$ node scripts-exec.js import-config
```
Now see the application running fine in the browser. To troubleshoot and
for more details, refer the document:

<http://wiki.openearth.community/wikidocs/Installation.pdf>

## 5 Storage

### 5.1 Volumes vs Persistent Volumes

There are two types of storage abstracts available with Kubernetes:
Volumes and Persistent Volumes.

Kubernetes Volume

-   has the same lifecycle as the containing pod

-   the associated volume is deleted if the pod is deleted

-   useful for storing temporary data that does not need to exist
    outside of the pod's lifecycle

Kubernetes Persistent Volume

-   available outside of the pod lifecycle

-   the associated volume will remain even after the pod is deleted

-   useful for storing permanent data

### 5.2 AWS EFS vs EBS

Both are persistent volumes that AWS provides.


| EFS                 | EBS                                                                    |
|--------------------------------|--------------------------------------------------------------------------------|
| Amazon EFS provides a shared file storage for use with compute instances in the AWS cloud and on premise servers.   |  Amazon EBS is a cloud block storage service that provides direct access from a single EC2 instance to a dedicated storage volume |  |
|       |       |
|Applications that require shared file access can use Amazon EFS for reliable file storage delivering high aggregate throughput to thousands of clients simultaneously|  Application that require persistent dedicated block access for a single host can use EBS as a high available and low-latency block storage solution.|
| | |
|EFS PV provides ReadWriteMany access mode.| EBS PV provide only ReadWriteOnce access mode|
| | |
|AN EFS file system can be accessed from multiple availability zones and it is the valuable for multi-AZ cluster.|EBS can be accessed by the host it is connected within the zone. EBS volume is automatically replicated within its vailability Zone to protect you from component failure,offering high availability and durability.|
| | |
|It is better to choose EFS when it is difficult to estimate the amount of storage the application will use because EFS is built to elastically scale. |Automatic scaling is not available in EBS but can scaled up down based on the need.|
|||
|More costly than EBS. |Cost-efficient.|
|||
|EFS is a file system; hence, it won’t support some applications such as databases that require block storage.| Can support all type of application.|
|||
|EFS doesn’t support any backup mechanism we need to setup backup manually | EBS on the other hand provides point-in-time snapshots of EBS volumes, which are backed up to Amazon S3 for long term durability.|
|||
|EFS doesn’t support snapshots.|Amazon EBS provides the ability to copy snapshots across AWS regions, enabling geographical expansion, data center migration, and disaster recovery providing flexibility and protecting for your business.|                                                       

### 5.3 Persistent Volume (PV)

Persistent Volume (PV) is a piece of storage and it is a cluster
resource like a node. This API object captures the details of the
implementation of the storage, be that NFS, iSCSI, or a
cloud-provider-specific storage system. Lifecycle of a PV is independent
of any pod that uses the PV.

Get the volumeID and availability-zone details from the admin.

**EBS Volume**:
```
apiVersion: v1 
kind: PersistentVolume 
metadata: 
  name: firstpv 
  labels: 
    type: amazonEBS 
spec: 
  capacity: 
  storage: 80Gi 
  accessModes: 
    - ReadWriteOnce 
  persistentVolumeReclaimPolicy: Retain 
  storageClassName: mystorage 
  awsElasticBlockStore: 
    volumeID: vol-0e058f2e3951b4762 
    fsType: ext4
```
Note:

-   If accessModes: *ReadWriteOnce* it means the volume can be mounted
    as read-write by a single node

**EFS Volume:**
```
apiVersion: v1 
kind: PersistentVolume 
metadata: 
  name: my-efs-pv 
  namespace: team-dev 
spec: 
  capacity: 
    storage: 12Gi 
  accessModes: 
    - ReadWriteMany 
  storageClassName: aws-efs 
  nfs: 
    server: <EFS server host>
    path: "/
```
Note:

-   If accessModes: *ReadWriteMany* - the volume can be mounted as
    read-write by many nodes
-   Amazon EFS extends NFS4. '*fs-e495e604.efs.us-east-1.amazonaws.com*'
    is the default EFS server that EKS is using
-   The advantage of using EFS over EBS is volume can be shared across
    nodes/availability zones.
-   Specifying Storage does not make any sense in EFS, but need to
    provide because storage is the mandatory parameter to create volume.

Save this to yaml file and execute the below command.
```
kubectl create -f firstpv.yaml -n
```
A persistent Volume will be created.

Note:

-   This command also can be executed only by admin

### 5.4 Persistent Volume Claim (PVC)

A Persistent Volume Claim (PVC) is a request for storage by a user. It
is similar to a pod. Pods consume node resources and PVCs consume PV
resources. Pods can request specific levels of resources (CPU and
Memory). Claims can request specific size and access modes (e.g., can be
mounted once read/write or many times read-only).

**Amazon EBS**:
```
apiVersion: v1 
kind: PersistentVolumeClaim 
metadata: 
  name: first-pv-claim 
  namespace: <your namespace> 
  labels: 
    app.kubernetes.io/name: myservice 
spec: 
  accessModes: 
  - ReadWriteOnce 
  storageClassName: mystorage 
  resources: 
    requests: 
    storage: 80Gi 
  selector: 
    matchLabels: 
        type: amazonEBS
```
**AWS EFS:**
```
apiVersion: v1 
kind: PersistentVolumeClaim 
metadata: 
  name: my-efs-claim 
  namespace: <your namespace> 
  labels: 
    app.kubernetes.io/name: myservice 
spec: 
  accessModes: 
  - ReadWriteMany 
  storageClassName: aws-efs 
  resources: 
    requests: 
      storage: 10Mi
```
Save this script to a yaml file and execute the below command
```
kubectl create -f first-pv-claim.yaml -n <your namespace>
```
Note: This command can be executed by user. Admin right not required for
this.

To see the available PVC in your namespace
```
kubectl get pvc -n <your namespace>
```
NOTE: AWS supported accessModes*:* '*ReadWriteOnce' (EBS) and
'ReadWriteMany' (EFS)*

### 5.5 Binding PV with PVC

An PVC can be bound with a PV with the help of Labels /Selectors

**Define a Label in PV**:
```
apiVersion: v1 
kind: PersistentVolume 
metadata: 
  name: firstpv 
  labels: 
    type: amazonVolume
```
**Use this Label in PVC :**
```
apiVersion: v1 
kind: PersistentVolumeClaim 
metadata: 
… 
spec: 
…. 
  selector: 
    matchLabels: 
        type: amazonVolume
```
**PV & PVC Lifecycle**:

A PVC to PV binding is a one-to-one mapping. If a user deletes a PVC in
active use by a pod, the PVC is not removed immediately. PVC removal is
postponed until the PVC is no longer actively used by any pods, and also
if admin deletes a PV that is bound to a PVC, the PV is not removed
immediately. PV removal is postponed until the PV is not bound to a PVC
any more.

### 5.6 Bind PVC to Pod

Need to specify the PVC name which was already created, mount path.
```
apiVersion: apps/v1beta2 
kind: Deployment 
metadata: 
  name: myservice 
  namespace: <your namespace> 
  labels: 
… 
spec: 
  replicas: 1 
  selector: 
  matchLabels: 
    app.kubernetes.io/name: myservice1 
  template: 
  metadata: 
    labels: 
        app.kubernetes.io/name: myservice1 
  spec: 
    containers: 
        - name: myservice1 
        image: <service_image_name> 
      ports: 
        - name: http 
        containerPort: 8080 
        …………………….. 
      volumeMounts: 
        - name: new-volume 
        mountPath: /tmp 
      volumes: 
        - name: new-volume 
          persistentVolumeClaim: 
            claimName: first-pv-claim
```
There is a limitation in AWS EBS that, a service can use the EBS volume
only if it is deployed in the same aws availability zone as the EBS
volume. This limitation can be eliminated with EFS, which provides
accesibility from different availability zones.

### 5.7 Static/Dynamic Provisioning of Volume

**Static Provisioning:** A volume will be created initially by
administrator and can bind it when required.

Steps for Static provisioning:

-   Make sure a volume will be created (eg: EBS volume / EFS volume) and
    get the volume ID availability zone of it. This step is applicable
    only for EBS volume. Refer [section](#aws-efs-vs-ebs)

-   Create Storage Class only when you want a customized StorageClass.
    Refer [section](#persistent-volume-pv)

    This step is optional. This should be created with admin privileges.

-   Create Persistent Volume Object. Refer
    [section](#persistent-volume-pv) . This can be created with admin
    privileges.

    Specify the Volume ID for EBS and EFS server host for EFS volume
    that was created. Specify the required StorageClass. If you do not
    specify, a default StorageClass specified in the cluster will be
    considered.

    Default StorageClass of EKS supports EBS volume. For EFS, must
    specify the StorageClassName (aws-efs)

    *awsElasticBlockStore*: specified in the above PV example is the AWS
    EBS volume plugin.

    *nfs*: specified in the above PV example is the NFS plugin. Amazon
    EFS works on top of NFS4 . Get the details of EFS server host and
    path and define in PV.

-   Create PVC. Refer [Section](#persistent-volume-claim-pvc)

-   Specify Labels/Selectors on PV & PVC to bind each other. Refer
    [Section](#binding-pv-with-pvc)

-   Create service deployment and bind the PVC with its name. Refer
    [Section](#bind-pvc-to-pod)

-   Now your service will be able to use the EBS/EFS volume created.

-   If a PVC was deleted, the bounded PV also will be deleted. But the
    data still persist in EBS volume.

    Deleting PV/PVC will just remove the binding to the EBS volume.

-   Incase if we need to rebind the volume, need to repeat above steps.

**Dynamic Provisioning**: A volume will be created only when requested
(through PVC)

Steps for Dynamic Provisioning:

-   Create PVC. Refer [Section](#persistent-volume-claim-pvc) .
    Specifying StorageClass is optional for EBS volume. For EFS volume,
    need to specify the StorageClassName. Available storageClassName for
    EFS in EKS platform is 'aws-efs'.

    Incase if you want to use a customized StrorageClass, create it
    first (Refer [Section](#persistent-volume-pv)) and refer the
    StorageClass name in PVC manifest file. When you create a PVC, it
    will be bond to the new Volume that created dynamically

    Check the status of the PVC with *kubectl get pvc -n \<namespace\>*

-   Create service deployment deploy it to the cluster. Refer
    [Section](#bind-pvc-to-pod) PVC will bind to the service and service
    can able to Read/Write data to the volume that got created
    dynamically

-   Volume will be deleted when PVC deleted and data will be lost

-   Make sure to keep PVC if data need to be persisted

-   One PVC can be shared by any number of services. In this way volume
    can be shared across services

### 5.8 Amazon S3 Storage

Amazon Simple Storage Service ([Amazon S3](https://aws.amazon.com/s3/))
is the largest and most performant, secure, and feature-rich object
storage service. With Amazon S3, organizations of all sizes and
industries can store any amount of data for any use case, including
applications, IoT, data lakes, analytics, backup and restore, archive,
and disaster recovery.

S3 consists of buckets. Each bucket has objects with different keys.
Technically S3 does not have folders or some structure, just a naming
convention.

For example:
```
s3://<bucketname>/foo/bar
```
Where \<bucketname\> is the "bucket", and "/foo/bar" is the key

#### 5.8.1 Platform Bucket 

Phoenix Platform creates one S3 bucket (called Platform Bucket)
available for all Pods in all namespaces. It has this naming
convention/structure:

```
S3://<Platform Bucket>/<Platform Bucket Home>/<namespace>/<data, tmp, jar>
```
Where:

-   **Platform Bucket** -- platform bucket name

-   **Platform Bucket Home** -- home key of the platform bucket

-   **namespace** - the name of the namespace in Platform

Each \<namespace\> contains three "subfolders" and you can create more
(if you have the permission)

-   **tmp/** is for temporary files, such as logs. Files in /tmp will be
    expired (get deleted) after 10 days.

-   **jar/** is for Spark jar files that your Spark jobs depend on

-   **data/** is for any other data

NOTE: There are no storage space limits

**Example:**

If you are working with a dev cluster "phoenix" in namespace "team-dev",
you can put your jar file "com.foo.PiCompute.jar" to this path:
```
s3://distplat/phoenix/team-dev/jar/com.foo.PiCompute.jar
```
where

-   "distplat" is the Platform Bucket
-   "phoenix/" is the Platform Bucket Home
-   "phoenix/team-dev/jar/com.foo.PiCompute.jar" is the key to the
    Object (jar file)

#### 5.8.2 Platform Bucket Env Variables

Platform bucket is specific to each Platform cluster. You can get
platform bucket environment variables in your deployed pods from
Platform global ConfigMap plat-config:
```
DPLAT_S3_BUCKET //Platform Bucket
DPLAT_S3_HOME //Platform Bucket Home
```
To use them in your pod, add the next section in Deployment part for
your manifest:
```
envFrom:
- configMapRef: 
    name: plat-config
```
After that, you may use it inside Docker container as
\$DPLAT\_S3\_BUCKET and \$DPLAT\_S3\_HOME:
```
“s3://”+ $DPLAT_S3_BUCKET +”/” + $DPLAT_S3_HOME + “team-dev/jar/com.foo.PiCompute.jar”
```
**NOTE:**

*These global environment variables are a part of any Phoenix Platform.
We recommend you to use these variables in your code instead of
hardcoding full path.*

#### 5.8.3 Run S3 Command 

You can run AWS S3 command in dpcli which has permission to access
Platform S3 Bucket:
```
docker run -it --rm -v $HOME/.aws:/root/.aws -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):/root/app -e AWS_PROFILE=<profile-name> distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 aws s3 …
```
Notice that you need to change \<profile-name\> with your AWS profile.

1.  Copy to S3:
```
docker run --rm … distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 aws s3 cp <local-path-to-file> <s3-path>
```
2.  List objects in "subfolder":
```
docker run --rm … distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 aws s3 ls <s3-path>/
```
**NOTE**: do not forget about final /.

3.  Get objects from S3:

```
docker run --rm … distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0 aws s3api get-object --bucket distplat --key phoenix/team-dev/data/my_images.tar.bz2
```

You may find more commands in AWS documentation.

#### 5.8.4 Work with S3 API in Pod

You can read/write S3 objects via S3 API in your Microservices. The
following is an example of using Java S3 API.

1.  Add Maven dependency:
```
<dependency> 
    <groupId>com.amazonaws</groupId> 
    <artifactId>aws-java-sdk-bom</artifactId> 
    <version>1.11.327</version> 
</dependency>
```

2.  Add Imports
```
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder; 
import com.amazonaws.services.s3.model.S3Object;
```
3.  Read from S3
``` java
@Path("/s3/read") 
@GET 
@Produces({ MediaType.TEXT_PLAIN }) 
@Override 
public String readFromS3() { 
    AmazonS3 s3client = AmazonS3ClientBuilder 
        .standard() 
        .withRegion(Regions.US_EAST_1) 
        .build(); 
    String key = System.getenv(“DPLAT_S3_HOME”) + "team-dev/tmp/visitors.txt"; 
    S3Object object = s3client.getObject(System.getenv(“DPLAT_S3_BUCKET”), key); 
    return object.getObjectContent() != null ? “success” : “fail”; 
}
```
4.  Write to S3
``` java
@Path("/s3/write") 
@POST 
@Produces({ MediaType.TEXT_PLAIN }) 
public String writeToS3() { 
    AmazonS3 s3client = AmazonS3ClientBuilder.standard() 
                        .withRegion(Regions.US_EAST_1) 
                        .build(); 
    File f = new File("/tmp/visitors.txt"); 
    try { 
    f.createNewFile(); 
    FileOutputStream fout = new FileOutputStream(f); 
    fout.write("Hello Visitor".getBytes()); 
    } catch (IOException e) { 
        e.printStackTrace(); 
        return "fail"; 
    } 
    String key = System.getenv(“DPLAT_S3_HOME”) + "team-dev/tmp/”+ f.getName(); 
    s3client.putObject(System.getenv(“DPLAT_S3_BUCKET”), key, f); 
    return "success"; 
}
```

## 6 ConfigMap

Configmap is a kubernetes object which isolates configuration artifacts
from image content and provides portability to containerized
applications.

### 6.1 Create ConfigMaps from literal Values.

Configure key:value pairs to config.

#### 6.1.1 Create ConfigMap with kubectl command.
```
kubectl create configmap newconfigmap --from-literal=prop1=value1 --from-literal=prop2=value2 -n <your_namespace>
```
View the created ConfigMap
```
[root@1923a84f0346 app]# kubectl get configmap newconfigmap -o yaml -n team-dev 
apiVersion: v1 
data: 
  prop1: value1 
  prop2: value2 
kind: ConfigMap 
metadata: 
  creationTimestamp: "2019-02-27T06:36:33Z" 
  name: newconfigmap 
  namespace: team-dev 
  resourceVersion: "2527241" 
  selfLink: /api/v1/namespaces/team-dev/configmaps/newconfigmap 
  uid: 05f90fd4-3a5a-11e9-95cd-0ee6c5fdcf10
```
#### 6.1.2 Create ConfigMap with yaml file

Create an yaml file as seen below.
```
apiVersion: v1 
kind: ConfigMap 
metadata: 
  name: newconfigmap 
  namespace: team-dev 
data: 
  prop1: value1 
  prop2: value2
```
Execute this command to create a ConfigMap
```
kubectl apply -f configmap.yaml -n <your_namespace>
```
To view the created configmap in the form of yaml
```
kubectl get configmap newconfigmap -o yaml -n <your_namespace>
```
### 6.2 Create ConfigMap from files

Map configuration files in local to ConfigMap object. Entire file
content can be copied to the ConfigMap.

#### 6.2.1 Create ConfigMap with kubectl command.
```
kubectl create configmap new-config --from-file=configs/ -n <your_namespace>
```
Note: File Path or folder path can be specified as the value for the
flag '\--from-file'

This will map all the files inside the folder will be mapped to a
ConfigMap name 'new-config'
```
kubectl describe configmap new-config -n <your_namespace>
```
```
Name: new-config 
Namespace: <your_namespace> 
Labels: <none> 
Annotations: kubectl.kubernetes.io/last-applied-configuration: 
        {"apiVersion":"v1","data":{"config.properties":"env.prop1: firstenvvalue\nenv.prop2:
secondenvvalue\n","config2.properties":"env2.prop1: f... 

Data 
==== 
config.properties: 
---- 
env.prop1: firstenvvalue 
env.prop2: secondenvvalue 
config2.properties: 
---- 
env2.prop1: firstenvvalue 
env2.prop2: secondenvvalue 
Events: <none>
```
Configuration Files and its content will be copied to ConfigMap object.

#### 6.2.2 Create ConfigMap with yaml file

Use the below template (\*.yaml file) to generate an yaml file for
configmap
```
apiVersion: v1 
kind: ConfigMap 
metadata: 
  name: "{{ .Chart.Name }}-configmap" 
data: 
  {{- (.Files.Glob “configs/*).AsConfig | nindent 2 }}
```
Include this template in phoenixmsp/templates folder and ConfigMap will
be created when the service deployed

### 6.3 Define Container env variables with ConfigMap
```
apiVersion: v1 
kind: Pod 
metadata: 
  name: dapi-test-pod 
spec: 
  containers: 
    - name: test-container 
    image: k8s.gcr.io/busybox 
    command: [ "/bin/sh", "-c", "env" ] 
    env: 
    # Define the environment variable 
    - name: SPECIAL_LEVEL_KEY 
    valueFrom: 
        configMapKeyRef: 
        # The ConfigMap containing the value you want to assign to SPECIAL_LEVEL_KEY 
            name: newconfigmap 
        # Specify the key associated with the value 
            key: prop1 
    restartPolicy: Never
```

Incase if it's required to define all ConfigMap items as env variables,

env:
```
env: 
# Define the environment variable 
- name: SPECIAL_LEVEL_KEY 
  envFrom: 
    configMapKeyRef: 
    # The ConfigMap containing the value you want to assign to SPECIAL_LEVEL_KEY 
      name: newconfigmap
```
### 6.4 Populate a Volume with data stored in ConfigMap
```
apiVersion: v1 
kind: Pod 
metadata: 
  name: dapi-test-pod 
spec: 
  containers: 
    - name: test-container 
    image: k8s.gcr.io/busybox 
    command: [ "/bin/sh", "-c", "ls /etc/config/" ] 
    volumeMounts: 
    - name: config-volume 
    mountPath: /etc/config 
  volumes:
  - name: config-volume 
  configMap: 
    # Provide the name of the ConfigMap containing the files you want 
    # to add to the container 
    name: newconfigmap 
restartPolicy: Never
```
This will load all the data (key:value pairs ) to the mapped volume

If there is a need to load some selected key: value pairs, define as
below
```
volumes: 
  - name: config-volume 
    configMap: 
      name: newconfigmap 
      items: 
      - key: prop1 
      path: keys
```
'path' where to save data from the mounted volume path location
"etc/config/"

Now, config values will be available at "etc/config/keys" of container

### 6.5 Replace ConfigMap

Configmap name 'newcofigmap' already created and this configmap can be
replaced with below command

**Replace command with key:value pairs as parameters**
```
kubectl create configmap newconfigmap --from-literal=newprop1=newvalue1 --from-literal=newprop2=newvalue2 --dry-run -o yaml -n team-dev | kubectl replace -f -
```
**Replace command with config file as parameter**
```
kubectl create configmap newconfigmap --from-file=configs/* --dry-run -o yaml -n team-dev | kubectl replace -f -
```
Here is the new content of 'newconfigmap'
```
[app]# kubectl get configmap newconfigmap -o yaml -n team-dev 
apiVersion: v1 
data: 
  newprop1: newvalue1 
  newprop2: newvalue2 
kind: ConfigMap 
metadata: 
  creationTimestamp: "2019-02-28T04:57:34Z" 
  name: newconfigmap 
  namespace: <namespace> 
  resourceVersion: "398970" 
  selfLink: /api/v1/namespaces/team-dev/configmaps/newconfigmap
  uid: 5ca1a097-3b15-11e9-82da-12fdf5a5098c
```
**Replace command with yaml file**

Create an configmap.yaml file
```
apiVersion: v1 
data: 
newprop3: newvalue3 
newprop4: newvalue4 
kind: ConfigMap 
metadata: 
name: newconfigmap 
namespace: <namespace>
```
execute this command to replace the existing configmap
```
kubectl get configmap newconfigmap -o yaml --dry-run -n <namespace> | kubectl replace -f configmap.yaml
```
### 6.6 ConfigMap Sync with pod

When there is a change/ replace ConfigMap data

-   If we are using ConfigMap data as env variable, in order to sync the
    ConfigMap changes with pod, service should be redeployed.

-   If ConfigMap data is referring a file, configmap changes will be
    synced automatically with pod with a little delay.

### 6.7 Global ConfigMap

## 7 Resource Request, Limits and Quotas

### 7.1 Resource Requests and Limits

Requests and limits are the mechanisms Kubernetes uses to control
resources such as CPU and memory. Requests are what the container is
guaranteed to get. If a container requests a resource, Kubernetes will
only schedule it on a node that can give it that resource. Limits, on
the other hand, make sure a container never goes above a certain value.
The container is only allowed to go up to the limit, and then it is
restricted.

It is important to remember that the limit can never be lower than the
request. If you try this, Kubernetes will throw an error and won't let
you run the container.

#### 7.1.1 CPU Requests and Limits

CPU resources defined in millicores. If your container needs two full
cores to run, you would put the value "2000m". If your container only
needs ¼ of a core, you would put a value of "250m".

One thing to keep in mind about CPU requests is that if you put in a
value larger than the core count of your biggest node, your pod will
never be scheduled.

#### 7.1.2 Memory Requests and Limits

Memory resources are defined in bytes. Normally, you give
a mebibyte value for memory (this is basically the same thing as a
megabyte), but you can give anything from bytes to petabytes.

Just like CPU, if you put in a memory request that is larger than the
amount of memory on your nodes, the pod will never be scheduled.

### 7.2 Resource Quota in Namespace 

When several users or groups share a cluster with a fixed number of
nodes, there is a problem that one group can use more than its fair
share of resources.

Resource quota is installed by Platform administrator in a namespace;
you can't modify it.

If resource quota is installed in your namespace, you can view it by
getting the ResourceQuota object:
```
kubectl get ResourceQuota -n <namespace> -o yaml
```
The following table explains the meaning of these resource quotas:

![](./media/image10.png)

### 7.3 Default Request and Limits

**If quota is enabled** in a namespace for compute resources like CPU
and memory, the default request and limits will apply to the deployment
if you don't specify. You can view the default resource request and
limit values by viewing LimitRange object in your namespace:
```
kubectl get LimitRange -n <namespace> -o yaml
```
If you don't like the default requests and limits you can specify
**them** in your deployment yaml file:[]{#_Default_Resource_Limits
.anchor}
```
apiVersion: apps/v1
kind: Deployment
metadata: 
  name: my-deployment 
  … 
spec: 
  … 
  template: 
    … 
    spec: 
    containers: 
    - name: myservice 
      image: myservice:1.7.9 
      ports: 
    - containerPort: 80 
    resources: 
    requests: 
      cpu: “500m” 
      memory: "512Mi" 
    limits: 
      cpu: "1000m" 
      memory: "1024Mi"
```

### 7.4 Configuring Quality of Service (QoS)

Configuring services to efficiently use the cluster resources by
assigning particular Quality of Service(QoS ) classes to pods.
Kubernetes uses QoS classes to make decisions about scheduling and
evicting pods.

**requests** -- the amount of the resource that the system will assign
to the container in pod.

**limits** -- the maximum amount of the resource the system will allow
the container in pod to use.

Scheduling happen based on the requests values and not based on the
limits.

In Values.yaml, we can specify resource requests and limits to the pods
in terms of CPU and Memory.

***Based on the request and limits in the pod configuration, the POD
will be in 3 different qualities of service categories: ***

**Guaranteed** 

-   Pods are considered top-priority and are guaranteed to not be killed
    until they exceed their limits. 

-   If limits and optionally requests (not equal to 0) are set for all
    resources across all containers and they are equal, then the pod is
    classified as Guaranteed. 

> Example: The Container has a memory limit and a memory request, both
> equal to 200 MiB. The Container has a CPU limit and a CPU request,
> both equal to 700 milliCPU:
```
resources: 
  limits: 
    cpu: 700m 
    memory: 200Mi 
  requests: 
    cpu: 700m 
    memory: 200Mi
```
**Burstable** 

-   Pods have some form of minimal resource guarantee, but can use more
    resources when available. Under system memory pressure, these
    containers are more likely to be killed once they exceed their
    requests and no Best-Effort pods exist. 

-   If requests and optionally limits are set (not equal to 0) for one
    or more resources across one or more containers, [and they are not
    equal]{.underline}, then the pod is classified as Burstable. When
    limits are not specified, they default to the node capacity. 

    Example: The Container has a memory limit of 200 MiB and a memory
    request of 100 MiB
```
resources: 
  limits: 
    memory: "200Mi" 
  requests: 
    memory: "100Mi"
```

**Best-Effort** 

-   Pods will be treated as lowest priority. Processes in these pods are
    the first to get killed if the system runs out of resources on a
    node. These containers can use any amount of free memory in the node
    though. 

-   If requests and limits are not set for all of the resources, across
    all containers, then the pod is classified as Best-Effort. 

    Example: The Container has no memory or CPU limits or requests

resources: {}

**Recommendation/Best Practice**

*  For services that are seldom needed/rarely called/do a quick process
    and then go back to idle, do not specify any request or limits. This
    is basically what we are doing today. 

*  For important services in your application, please specify the
    request and limit values. For services that do not need to burst,
    specify both request and limit, and make sure they are the same
    value. This will give the most consistent and stable performance for
    your service. 

*  For services that need to burst, i.e. use some amount of cpu/memory
    under normal load, but need more cpu / memory under heavy load, then
    set limits higher than request.  

*  Keep in mind that your service will be potentially killed if it goes
    above the limit. 

*  Keep in mind that having several instances of the service is usually
    better than growing 1 instance of the service to serve more users. 

*  Specify the values in your helm chart for the production
    environment. Override these values when you deploy the services in
    the --dev and --qa namespaces

*  Your service should be performing well with 0.5 cpu. It is not
    computing 100% of the time.

*  For memory, if you are using java, make sure the jvm -Xms and -Xmx
    flags don't contradict/interfere with the values set at the pod
    level. 

*  Be conservative in your resource requests. If you are not, then we
    will end up with an environment grossly oversized compared to the
    needs.

 

## 8 Helm Chart

### 8.1 Overview

An introduction to Helm, the package manager for Kubernetes.

Deploying Applications to Kubernetes can be complex. Setting up a single
application can involve creating multiple interdependent Kubernetes
resources-such as pods, services, deployments and replica sets-each
requiring you to write a detailed YAML manifest file.

Helm is a package manager for Kubernetes that allows developers and
operators to easily package, configure and deploy applications and
services onto Kubernetes cluster.

Helm provides this functionality through the following components:

-   A command line tool, helm, which provides the user interface to all
    > Helm functionality.

-   A companion server component, tiller, which runs on your Kubernetes
    > cluster, listens for commands from helm, and handles the
    > configuration and deployment of software releases on the cluster.

-   The Helm packaging format, called *charts*.

-   Charts are Helm packages that contain at least two things:

    -   A description of the package (Chart.yaml)

    -   One or more templates, which contain Kubernetes manifest files

-   Charts can be stored on disk, or fetched from remote chart
    > repositories.

### 8.2 Prepare Helm Chart

To prepare helm chart, we need to first clone the below url to generate
a template
```
git clone -b release-3.2 <https://git.openearth.community/DistServices/helm-deploy.git>
cd helm-deploy
```
You can find a helm chart name 'phoenixmsp'.

*  Modify the folder name as your chart name ex: 'phoenix', modify
    chart name in Chart.yaml same as the folder name.

*  Modify values in Values.yaml with required values like
    image.repository, image.tag, global.cluster.hosts etc

    Find more details about these values in
    [section](#_values.yaml_–_Detail)

*  Rename phoenix.yaml in templates folder to <YOUR_SERVICE\>.yaml

> Example: If service name is 'helloservice' then rename phoenix.yaml to
> 'helloservice.yaml'

Now the helm chart is ready.

Note: Using chart name '**phoenix'** for illustration

#### 8.2.1 Explain Charts.yaml

Chart.yaml defines the chart name,version,app version and description of
your helm chart.You can customize them per your requirements.

name: Change the default value 'phoenixmsp' to your chart name.

version:Set version number for your helm chart.

appVersion: Set your application version.this useually be your
application service version number.

#### 8.2.2 Explain Values.yaml

**global.cluster.hosts**: Set the cluster host name // eg:distplat.landmarksoftware.io

**global.env.production**: Set the deployment to production or non-production namespaces.default is true for production namespace.

**global.cloud.vendor**:  Set the cloud vendor name aws/azure

**global.node.selector**:  Set the node type linux/windows

**productName**:  It is mandatory and should specify application or product name that the service belongs to, will be used in NewRelic configuration for the service.

**replicaCount**: Set the number of pod instances to be created for the deployment.default is 1. // eg: 2

**image.repository**: Set microservice docker image name along with repository url, but without version

// eg:distplat-docker-milestone.hub.ienergycloud.io/distarch/com.lgc.dist.core.msp.example.helloservice

**image.tag**: Set the Image version // eg: 0.8.0-SNAPSHOT

**Image.pullPolicy**: Instruction to pull an image from repository. // eg Always, Never, IfNotPresent

**fullnameOverride**: Default value is empty. Set this variable only if you need to override the resource name in production namespace

**initcontainers.volumeMounts**: Define volume mounts for init container.

**service.type**: Required type of service. Possible values are ClusterIP, NodePort, LoadBalancer, ExternalName.Default is 'clusterIP' for phoenix
platform cluster deployments.

**service.port**: Defines service port for your application.Default is port 80.

**service.containerPort**: Defines container port.Default is 8080.Change if it is different from 8080.

**service.annotations**:  To add annotations to the service.

**service.labels**:  To add labels to the service.

**configmap.env**: Defines configmap from literal environment variables. Default it is empty.

**configmap.file**: Defines configmap from file mapped to volume. Default it is empty.

**secret.env**: Defines secrets as env variables. Default it is empty.

**secret.file**: Defines secrets in file. Default it is empty.

**blockStorage**: Defines ebs volume mountpath. Default it is empty.

**fileStorage**: Defines efs volume mountpath. Default it is empty.

**transientvolumes**:  To create transient or emptyDir volume for pod life cycle

**env**: This attribute will provide environment variables to be added in deployment.

**resources.requests.cpu**: cpu  resources that can be guaranteed to a container //eg: 100m (100 millicores)

**resources.requests.memory**: Memory in bytes that can be allotted to the container //eg: 128Mi (128 Mega Bytes)

**resources.limits.cpu**: Max cpu resources that can be allotted to container and should not exceed the value. This value should always be equal or more than request cpu value //eg: 200m (200 millicores)

**resources.limits.memory**: Max Memory in bytes that can be allotted to the container and should not exceed this value. This value should always be equal or more than request memory value. //eg: 256Mi (256 Mega Bytes)

**command**: it holds the script file name which the script that will be executed at the time of container start.

eg: [“/bin/sh, /javarun/start.sh”]

**args**: command line arguments to be passed to the micro service. //eg: [value1, value2,…]

**routes**:  To add more http routes for the application

**extraHttpRules**:  To add more http rules like headers, redirect, timeout etc.

**serviceMonitor**: To provide metrics path of the application

**webSocketUpgrade**: To specify whether the application is websocket applicaion. (true/false)

**livenessProbe**:   path to confirm the application liveness.

**redinessProbe**: path to confirm whether the application is ready to accept the traffic.

**hooks**: To add helm hooks to the application. Need to provide "schema image" details, "env variable"s required for hooks and other hook specific details under this.

**web-app**:  All the web-app specific details like web-annotations, keycloak details moved under this element


#### 8.2.3 Explain <SERVICE\>.yaml

Default file name is 'phoenix.yaml' in templates folder.Rename 'phoenix.yaml' to <SERVICE\>.yaml.

Uncomment the sections per your requirement.

Example: if you want to enable authorization feature for the service, uncomment the section "\_authorization.tpl".

Follow further steps per comments and documentation in the sections when you enable features in yaml file.

#### 8.2.4 Helm Chart Naming

There are three different naming properties to set in helm chart: Chart
name, Release name and Service name.

-   **Chart name**: Chart name is to identify the chart used to deploy
    the application. 'name' attribute defines the helm chart name in
    charts.yaml. This must be set before running helm install command.

-   **Release name**: Release name is to identify the release of
    application. You must specify the release name by setting 
    in helm install command.
```
Example: helm install phoenix distplat-helm/phoenixmsp --version 2.0 --namespace team-dev
```
> Run 'helm ls \--namespace=\<namespace\>' will show all the
> release names, chart name and chart version used.

-   **Resource name**:

    -   If "global.env.production" is false (in a dev namespace), the
        resource name is \<Release name\>-\<Chart name\>

    -   If "global.env.production" is true (in a production namespace),
        the resource name is \<Chart name\>, or equals to
        "fullnameOverride" if it is not empty

### 8.3 Package and Publish Helm Chart

#### 8.3.1 Listing Repositories

To execute helm commands, first connect to dpcli image.
```
docker run -it --rm -v $HOME/.aws:/root/.aws -v $(pwd):/root/app -e AWS_PROFILE=team-dev distservices-docker.repo.openearth.io/distarch/dpcli-distplat:2.0
```
If you run the "helm repo list", you'll see your repo listed
```
[app]# helm repo list
NAME            URL 
distplat-helm   https://hub.ienergycloud.io/artifactory/distplat-helm/
```
#### 8.3.2 Packaging Helm Chart

To package helm chart, one needs to execute the helm package command
from "helm-deploy" directory. In the helm-deploy directory - you should
have a directory with your service name. If you have made all the
required yaml changes in cloned phoenixmsp directory then rename
phoenixmsp directory to your service name. helm-deploy/SERVICENAME
directory needs to contain Chart.yaml file with same name as your
service name and version as your service version. Once you have all the
yaml configurations in place, execute the following command in
helm-deploy directory
```
# helm package SERVICENAME
```
Helm package command will create SERVICENAME-VERSION.tgz packaged file
in helm-deploy directory, it uses name and version from the metadata
section of the Chart.yaml file.

#### 8.3.3 Pushing Helm Chart to Repository

To push the chart to the repository, we need packaged file as mentioned
in the above step. Now you can push it to distplat-helm repository using
the following jfrog cli command.
```
[app]# jfrog rt u SERVICENAME-VERSION.tgz distplat-helm 
[Info] [Thread 2] Uploading artifact: SERVICENAME-VERSION.tgz 
{ 
"status": "success", 
"totals": { 
"success": 1, 
"failure": 0 
} 
}
```

Once published, you can check out the registry at this
[link](https://hub.ienergycloud.io/artifactory/distplat-helm/) using
iEnergy credentials same as used in section 3.5. Following is the
distplat-helm artifactory page snapshot:

![](./media/image11.png)

#### 8.3.4 Delete Helm Chart from Repository

**Deleting the helm chart from the repository is not recommended. Create
a new version of helm chart upon changes to helm chart.**
```
[app]# jfrog rt del distplat-helm/<nameofchart>-<version>.tgz
```
### 8.4 Deploy Helm Chart

#### 8.4.1 Searching Helm Charts

The state of the repository gets cached on your disk. When you update
the remote repository, or access it using search/fetch commands, run the
following update command first to retrieve the latest updates before you
access it.
```
# helm repo update
```
When searching for a chart using the helm command as follows for example
for phoenix chart, you will see the latest chart listed.
```
[root@fd4ae66586ca app]# helm search repo distplat-helm/phoenix 
NAME                  CHART VERSION APP VERSION DESCRIPTION 
distplat-helm/phoenix 0.1.6         1.1         A Helm chart for Kubernetes
```

For searching all versions of a chart, one could use the following two
commands:
```
# helm search repo -l distplat-helm/CHARTNAME
# helm search repo distplat-helm/CHARTNAME --versions
```

For searching development versions of a chart like '0.0.0-0', one could use the following two
commands. If --version is set --devel will be ignored

```
# helm search repo distplat-helm/CHARTNAME --devel
```

Command for searching a specific chart version:
```
# helm search repo distplat-helm/CHARTNAME --version=VERSION
```
#### 8.4.2 Deploying Helm Chart

##### 8.4.2.1 Deploy Backend Service Helm Chart

**Deploy a backend service "fooservice" to dev namespace "team-dev":** 
```
[root@03a6784a1a7b app]# helm install myrelease distplat-helm/fooservice --version 2.0 --namespace team-dev --set global.env.production=false,global.cluster.hosts= distplat.landmarksoftware.io,global.cloud.vendor=aws
```
-   The "fooservice" helm chart is released as "myrelease" (release name) in namespace "team-dev"
-   The pod name is "myrelease-fooservice-xxxxxxx"
-   The cloud vendor name is aws. Can also use the "azure" if user wants to deploy the application in azure cluster
    Use below command to know the pod status.
```
[root@03a6784a1a7b app]# Kubectl get pods –n team-dev
```
-   The service can be accessed by <https://distplat.landmarksoftware.io/services/myrelease-fooservice/>

***Deploy a backend service "fooservice" to production namespace "team":***
```
[root@03a6784a1a7b app]# helm install fooservice distplat-helm/fooservice --version 2.0 --namespace team --set global.cluster.hosts=distplat.landmarksoftware.io, global.env.production=false,global.cloud.vendor=aws
```
-   The "fooservice" helm chart is released as "fooservice" (release name) in namespace "team"
-   The pod name is "fooservice-xxxxxxx"
-   The cloud vendor name is aws. Can also use the "azure" if user wants to deploy the application in azure cluster
    Use below command to know the pod status.
```
[root@03a6784a1a7b app]# Kubectl get pods –n team-dev
```
-   The service can be accessed by <https://distplat.landmarksoftware.io/services/fooservice/>

##### 8.4.2.2 Deploy Web Application Helm Chart

**Deploy a web application "myapp" to dev namespace "team-dev":**
```
[root@03a6784a1a7b app]# helm install myrelease distplat-helm/myapp --version 2.0 --namespace team-dev --set global.env.production=false,global.cluster.hosts= distplat.landmarksoftware.io,global.cloud.vendor=aws
```
-   The "myapp" helm chart is released as "myrelease" (release name) in namespace "team-dev"
-   The pod name is "myrelease-myapp-xxxxxxx"
-   The cloud vendor name is aws. Can also use the "azure" if user wants to deploy the application in azure cluster
    Use below command to know the pod status.
```
[root@03a6784a1a7b app]# Kubectl get pods –n team-dev
```
-   The application can be accessed by <https://myrelease-myapp.distplat.landmarksoftware.io/>

***Deploy a web application "myapp" to production namespace "team":***
```
[root@03a6784a1a7b app]# helm install myapp distplat-helm/myapp --version 2.0 --namespace team --set global.cluster.hosts=distplat.landmarksoftware.io, global.env.production=false,global.cloud.vendor=aws
```
-   The "myapp" helm chart is released as "myapp" (release name) in namespace "team"
-   The pod name is "myapp-xxxxxxx"
-   The cloud vendor name is aws. Can also use the "azure" if user wants to deploy the application in azure cluster
    Use below command to know the pod status.
```
[root@03a6784a1a7b app]# kubectl get pods –n team-dev
```
-   The service can be accessed by <https://myapp.distplat.landmarksoftware.io/>

##### 8.4.2.3 Deploy a diagnostic service in the Platform

if the user wants to deploy a backend service as a diagnostic service, user can add a value `test` for the key `app_type` in Values.yaml of your helm chart.<br>
`app_type` can have one of the following values to determine the service type.

| app_type     | Description   | Url formation  | Example|
| ------------ | ------------ | ------------ | ------------- |
| web-app      | a web application  |  https://_servicename_._cluster_dns/ | https://myapp.distplat.landmarksoftware.io |
| app          |  a backend service | https://_cluster_dns_/services/_servicename  | https://distplat.landmarksoftware.io/services/myapp |
| test         | a diagnostic backend service |   https://_cluster_dns_/services/_servicename | https://distplat.landmarksoftware.io/services/maypp |

<br>
It can have multiple values as well.<br>
Example: `app_type: "app,test"` where this service will be considered as a `diagnostic app`<br>
How to install a `diagnostic` backend service using helm chart ?
```
helm install helloservice distplat-helm/helloservice --version 3.1 --namespace dp-dev --set global.cluster.hosts=distplat.landmarksoftware.io --set global.env.production=false --set app_type="app\,test"
```
`NOTE:` Helm considers anything after comma (,) as a new key. So its important to escape comma while supplying multiple values in `app_type="app\,test"` <br>
Once the diagnostic service deployment is successful, this service will be listed in `devportal` of the platform under menu `Applications -> Diagnostic`. <br>

![](./media/diagnostic.png)

Click of the diagnostic service in the list which will take user to the swagger page(click Interactive api menu) of the service like below <br>
![](./media/swagger_helloservice.png)


#### 8.4.3 Update the Deployment with Helm Chart 

Microservice is upgraded by executing the below command
```
[root@03a6784a1a7b app]# helm upgrade <release_name> phoenixmsp --force --namespace <namespace> --set global.cluster.hosts=distplat.landmarksoftware.io, global.env.production=false
```
Note: You can also specify the remote repo <repo name/chartname\>.

We can set other parameters of values.yaml in this command (Like replicaCount, configmap, pvc..)

After successful upgrade, we can notice

-   A new pod will be created and is in running state
-   The existing pod will be terminated

In case there are no changes in deployment but the microservice has been
modified and we need to redeploy the microservice with same image
version, then we need to use the above command.

#### 8.4.4 Helm Upgrade with Option to Install

Helm upgrade can install release if release not installed already. If
release is there installed already, helm will proceed 'upgrade' action.
```
[root@03a6784a1a7b app]# helm upgrade <release_name> <helmchart> --install --force --namespace=<namespace> --set global.cluster.hosts=distplat.landmarksoftware.io, global.env.production=false
```    
### 8.4.5 Rollback Deployed Microservices

In order to rollback to any of the previous deployment
```
[root@03a6784a1a7b app]# helm rollback <release_name> <revision number> --namespace=<namespace>
```
**Steps to roll back the deployment**

* In order to rollback, we need to check the history of the service so
that we can roll back to the version which we need .This is done by the
below command.
```
helm history releasename --namespace=<namespace>
```
```
helm history releasename --namespace=team-dev2 
REVISION    UPDATED              STATUS         CHART      DESCRIPTION 
1      Thu Mar 14 10:10:01 2019  SUPERSEDED phoenix-0.1.6 Install complete 
2      Thu Mar 14 10:43:43 2019  SUPERSEDED phoenix-0.1.7 Upgrade complete 
3      Thu Mar 14 10:53:11 2019  DEPLOYED   phoenix-0.1.7 Upgrade complete
```

* Once we get the history, we can roll back to the previous version using our rollback command
```
helm rollback releasename 1 --namespace=team-dev2
Rollback was a success! Happy Helming!
```

* You can check if the rollback happened by using the same history
command
```
[root@170d02fe3be3 app]# helm history releasename --namespace=team-dev2 
REVISION    UPDATED             STATUS        CHART         DESCRIPTION 
1   Thu Mar 14 10:10:01 2019    SUPERSEDED  phoenix-0.1.6   Install complete 
2   Thu Mar 14 10:43:43 2019    SUPERSEDED  phoenix-0.1.7   Upgrade complete 
3   Thu Mar 14 10:53:11 2019    SUPERSEDED  phoenix-0.1.7   Upgrade complete 
4   Thu Mar 14 11:35:34 2019    DEPLOYED    phoenix-0.1.6   Rollback to 1
```

### 8.5 Umbrella Helm Charts

#### 8.5.1 Overview

In the above sections, we have seen Helm chart as a deployment package
per service. Large-scale applications are composed of one or more of
these services.

Helm chart can be used to deploy large-scale application composed of
several services. In this case, a Single umbrella chart with multiple
subcharts can be used to deploy all the services at one time.

##### 8.5.1.1 Subcharts

To this point we have been working only with one chart. But charts can
have dependencies, called *subcharts*, that also have their own values
and templates.

*  A subchart is considered \"stand-alone\", which means a subchart can
    never explicitly depend on its parent chart.

*  For that reason, a subchart cannot access the values of its parent.

*  A parent chart can override values for subcharts.

*  Helm has a concept of *global values* that can be accessed by all
    charts.

##### 8.5.1.2 Global Variables

Global values are values that can be accessed from any chart or subchart
by exactly the same name. Globals require explicit declaration. You
can\'t use an existing non-global as if it were a global.

Global values are declared at top level starting with keyword 'global:'
```
Example: 
global:
  cluster:
    hosts:
  env:
    production: true
    key1: value1  // can add more env variables and values as key/value pairs
  cloud:
    vendor: aws  // possible values aws/ azure
  node:
    selector: windows  // possible values windows/linux  
```

The Values data type has a reserved section called Values.global where global values can be set.

In values.yaml we use
*  'global.cluster.hosts' to set the cluster name. This variable is used in subcharts to know the cluster name for deployment. The default value of this is empty and can set this value with below command.
*  'global.env.production' to specify whether the deployment environment is production or not. (true/false). default is 'true'
*  'global.cloud.vendor' to specify the cloud vendor name for the cluster that we are using for deployment.
 possible values are aws, azure. Default value is "aws"
 * 'global.node.selector' to specify the node type to deploy the application. Possible values are linux, windows. Default is linux
 
```
[root@03a6784a1a7b app]# helm install fooservice distplat-helm/fooservice --version 2.0 --namespace team --set global.cluster.hosts=distplat.landmarksoftware.io, global.env.production=false,global.cloud.vendor=aws,global.node.selector=linux
```

#### 8.5.2 Umbrella Helm Chart All-In-One Pattern

An all-in-one umbrella chart pattern is used to deploy a large scale application composed of multiple services.

##### 8.5.2.1 When Do We Use This Pattern

Use all-in-one umbrella chart pattern when all the services composed for the application share same templates but different deployment attributes per service in chart.yaml and values.yaml.

##### 8.5.2.2 Folder Structure

To prepare helm chart, we need to first clone the below url to generate
a template
```
git clone -b release-3.2 https://git.openearth.community/DistServices/helm-deploy.git ./umbrella-all-in-one
cd umbrella-all-in-one
```
Make the modifications per the following folder structure.

The following is the structure at parent level all-in-one umbrella
chart.

![](./media/image12.png)

In this pattern, all the services share common templates in 'templates'
folder at parent level but different chart.yaml and values.yaml in own
chart directory per service.

-   chart.yaml defines chart name for umbrella chart.
-   values.yaml defines the attributes for umbrella chart.
-   'charts/' folder will have all the subcharts for services.subchart
    contains chart.yaml,values.yaml and templates folder defines
    subchart deployment.
-   templates folder will have templates shared across all the services.

Here is an example of complete structure of umbrella-all-in-one helm
chart:

![](./media/image13.png)

-   /charts/helloservice is folder for helloservice subchart.
-   /charts/helloservice/templates/helloservice.yaml -- defines
    helloservice deployment
-   /charts/helloservice/Chart.yaml -- helloservice chart attributes
-   /charts/helloservice/Values.yaml -- defines attributes in
    values.yaml for helloservice subchart

##### 8.5.2.3 Deploying Umbrella Helm Chart

Deploying umbrella helm chart is same as deploying a helm chart. Use the following command to install all-in-one umbrella helm chart to deploy an application composed of several services.
```
[root@03a6784a1a7b app]# helm install <release_name> umbrella-all-in-one --set global.cluster.hosts=<clustername>.landmarksoftware.io,global.env.production=false  --namespace <namespace>
```    
#### 8.5.3 Umbrella Helm Chart – Managed Dependency Pattern

If more control over dependencies is desired, these dependencies can be expressed explicitly by copying the
dependency charts into the charts/ directory.
A dependency can be either a chart archive (foo-1.2.3.tgz) or an unpacked chart directory. But its name cannot start
with _ or .. Such files are ignored by the chart loader.

##### 8.5.3.1 When do we use this pattern

Use Umbrella helm chart Managed Dependency pattern when helm chart for composed services are available. This
pattern gives nested umbrella of umbrella structure.

##### 8.5.3.2 Folder structure

To prepare helm chart, we need to first clone the below url to generate a template
```
git clone –b release-2.0 https://git.openearth.community/DistServices/helm-deploy.git ./umbrella-dependency-pattern
cd umbrella-dependency-pattern
```
Add charts folder where all the children will go.

The following is the folder structure for umbrella helmchart-managed-dependency-pattern (example).

![](./media/imagenew.png)

-   Chart.yaml – defines attributes specific to umbrella helm chart
-   values.yaml – defines attributes specific to umbrella helm chart and also override attributes for specific dependency helm chart.
-   ‘charts/’ folder will have all the subcharts for services. subchart contains chart.yaml, values.yaml and templates folder defines subchart deployment.
-   templates folder will have templates shared across all the services.

In the above depicted example, we are showing 2 levels of parent hierarchy. Parent “msp-microservices” has
“windows” and “linux” as its children. They subsequently have “microservice-helloservice” and “microservice-
kubeservice” as their child respectively. You can add many services as per your requirement.
The parent values are inherited by the children. Parents can also override the values in children. For example, in the
above depicted hierarchy the root parent can use –

```
linux:
 microservice-kubeservice:
 image:
    tag: "2.0.0"
```
An intermediate parent like “windows” would use –

```
microservice-helloservice:
 image:
  tag: "2.0.0"
```
##### 8.5.3.3 Deploying Umbrella Helm Chart

Deploy the umbrella helm chart

```
[root@03a6784a1a7b app]# helm install <release_name> umbrella-managed-dependency-pattern --set
global.cluster.hosts=<clustername>.landmarksoftware.io, global.env.production=false --namespace <namespace>
```

#### 8.5.4 Naming Conventions for Services -- Multi Namespace and Multi Service

-   **Chart Name**: Helm chart name used to deploy application
    (Chart.yaml \-- name attribute)

apiVersion: v1 
appVersion: "1.0" 
description: A Backendmsp Helm chart for Kubernetes testing 
name: backendmsp 
version: "2.0"

-   **Release Name**: Release name for your application/service (\--name
    parameter in 'helm install/template')
```
helm install dev backendmsp --namespace=dp-dev
```
Release name is 'dev' in the above example deployment.

-   **Resource Name(or fullname)**: Name of k8s objects Service,
    deployment, virtualservice etc.

Default Resource Name is in pattern Release Name--Chart Name

Chart Name = backendmsp

Release Name = dev

**Resource Name = dev-backendmsp**

**Phoenixmsp templates values.yaml provides option to deploy to
production and non-production namespaces with property
global.env.production.**

**For Production namespace deployment:**

**By default global.env.production is true.**
```
helm install <releasename> backendmsp –set global.cluster.hosts=distplat.landmarksoftware.io --namespace=dp-dev
```
**For non-production deployments:**

**When global.env.production is false, it is non-production deployment
and resource name would be pattern 'dev-backendmsp'**
```
helm install dev backendmsp –set global.cluster.hosts=distplat.landmarksoftware.io, global.env.production=false --namespace=dp-dev
```
#### 8.5.5 Fetching the Helm Chart

You can fetch the chart locally using
```
[root@fd4ae66586ca app]# helm fetch distplat-helm/phoenix
```
This will fetch the tgz file from distplat-helm repo to your current
directory, which can be extracted by using below command

The above command fetches the latest chart version, for fetching a
specific version use the following command:
```
# helm fetch displat-helm/CHARTNAME --version=VERSION
```
```
[root@fd4ae66586ca app]# tar -xvzf phoenix-0.1.6.tgz
```
Once you fetch the charts locally, one can

1.  Untar into helm charts directory
2.  Make necessary modifications in chart.yaml and values.yaml if needed
    -   Examples: set image.repository,image.tag,global.cluster.hosts
        and so on

#### 8.5.6 List All The Helm Chart Service Deployments

The following command lists all the helm charts releases in the
namespace.
```
[root@03a6784a1a7b app]# helm ls --namespace=<namespace>
```
#### 8.5.7 Update Umbrella Helm Chart

Updating umbrella helm chart is not different from updating helm chart.
```
[root@03a6784a1a7b app]# helm upgrade <release_name> phoenixmsp --force --namespace <namespace> –set global.cluster.hosts=<clustername>.landmarksoftware.io, global.env.production=false
```
Note: You can also specify the remote repo <repo name/chartname\>.

We can set other parameters of values.yaml in this command (Like replicaCount, configmap, pvc..)

Helm is smart enough to identify and deploy only the modified
dependencies by terminating existing deployment.

After successful upgrade, we can notice

-   A new pod will be created and is in running state
-   The existing pod will be terminated

#### 8.5.8 Delete Helm Chart Deployment 

Delete the helm deployment for a service using the following command
```
[root@03a6784a1a7b app]# helm uninstall <release_name> --namespace=<namespace>
```
#### 8.5.9 Dry Run The Helm Chart

Run the deployment Plan With Dry-Run option.Release name has to be
unique.
```
[root@03a6784a1a7b app]# helm install releasename <helmchart> –set global.cluster.hosts=<clustername>.landmarksoftware.io,global.env.production=false  --namespace=<namespace> --dry-run --debug
```
##### 8.5.9.1 Recommendations

Use 'Release name' to differentiate same service deployed multiple
times.

Release name has to **unique**

-   **across your own namespaces for a service**

-   **within namespace**

**DEV & QA:**

Recommendation for DEV & QA services to be named with unique 'release
name'

\- multi deployment of same service in multiple namespace

\- multi deployment of same service in same namespace

BackendMSP examples:

BACKEND:

DEV or QA: Here dev and dev1 are two release names used differentiate
the same service deployed in same namespace.
```
helm install dev backendmsp --namespace=dp-dev --set global.env.production=false,global.cluster.hosts=distplat.landmarksoftware.io 
https://phoenix.landmarksoftware.io/services/dev-backendmsp/ 
helm install dev1 backendmsp --namespace=dp-dev --set global.env.production=false,global.cluster.hosts=distplat.landmarksoftware.io 
https://p hoenix.landmarksoftware.io/services/dev1-backendmsp/
```
**[Here are the list of release names 'dev' and 'dev1' for same service
backendmsp.]{.underline}**

![](./media/image15.png)

**PROD:**
```
helm install backendmsp --namespace=dp-prod --set global.cluster.hosts=distplat.landmarksoftware.io
https://phoenix.landmarksoftware.io/services/backendmsp/
```
Here is example helm chart for release name in PROD.

![](./media/image16.png)

## 9 Microservice End User Security (Authentication & Authorization)

Phoenix platform Microservices Authentication & Authorization is
implemented by Istio service mesh with integration of identity service
Keycloak (DS Security). The end user credential supported by
authentication policy is JWT.

Your Microservices are not required to implement any end user security
check, such as JWT validation and role based access control of the
service endpoints. End user security can be configured at the
Microservice deployment time.

There are common scenarios for securing a Microservice in a Phoenix
platform:

1.  No authentication and no authorization -- this is the default in a
    dev deployment, but not supported in production

2.  Authentication enabled but no authorization

3.  Authentication enabled with predefined roles

4.  Authentication enabled with custom roles

We assume user "demo" with password "demo" is configured in Keycloak in
the following sections. See Keycloak manual for managing users and
clients.

### 9.1 Deploy a Service With Security

NOTE: Deploying a Microservice without security is only supported in a
dev platform cluster. Microservice end user security is always required
in a production cluster and production type namespaces in development
cluster.

If you follow the Quick Start in this guide, your deployed service has
no end user security enabled, meaning anyone can access your service.

### 9.2 Production Namespaces

**Production namespaces enforces a JWT authentication for all the
services deployed. You need a valid JWT token to access deployed
service.**

Following these steps to test JWT Authentication for a service using
'curl' utility:

To test the service:

*  Get a valid JWT token
```
sudo yum install -y jq && 
TOKEN=$(curl -X POST https://<cluster name>.landmarksoftware.io/auth/realms/DecisionSpace_Integration_Server/protocol/openid-connect/token -H "Cache-Control: no-cache" -H "Content-Type: application/x-www-form-urlencoded" -d 'username=centos&password=centos&grant_type=password&client_id=service' | jq -r .access_token)
```
*  Pass the token as a bearer when make a call to the endpoint
```
curl --header "Authorization: Bearer $TOKEN" -X GET --header 'Accept: text/plain' 'https://<cluster>/services/<service>/<endpoint>'
```

### 9.3 Modheader extension plugin

Another way to Test the services deployed in production namespace is by
adding 'modheader' plugin extension for chrome or firefox browser.

The following is example installation of modheader plugin in firefox and
testing helloservice.

#### 9.3.1 Get modheader extension

Open firefox browser and enter the following url to get the 'modheader'
plugin

<https://addons.mozilla.org/en-US/firefox/addon/modheader-firefox/?src=search>

#### 9.3.2 Install modheader extension

![](./media/image17.png)

Click on 'add' to complete modheader installation.

![](./media/image18.png)

![](./media/image19.png)

#### 9.3.3 Get valid JWT Token 

Get the valid JWT token by going to dev portal

For distplat cluster,link is <https://dev.distplat.landmarksoftware.io>
and go to 'security' click on 'Access Token' link.

![](./media/image20.png)

Enter 'demo' for username and password as default unless you have
different credentials provided.

![C:\\Users\\H185119\\AppData\\Local\\Microsoft\\Windows\\INetCache\\Content.Word\\firefox-addon4.PNG](./media/image21.png)

Token has expiry time you can see the 'Expiring on:' in the screenshot below. Token expiry time is 5 minutes. After 5 minutes, you need to get new token and use.

![](./media/image22.png)

#### 9.3.4 Authorization Header setup 

Go to browser and 'open new tab' and click on 'modheader' extension icon in the menu bar.

![](./media/image23.png)

Setup 'Authorization' token header as shown in the following picture.
Copy the token with prefix 'Bearer' in the value.

![](./media/image24.png)

You save 'profile' and add 'filters' in the extension by clock on the
'+' icon.

![](./media/image25.png)

#### 9.3.5 Accessing service

Now type the url in the tab and try access the endpoints.

![](./media/image26.png)

### 9.4 Enable Authorization with User Defined Client and Roles

TODO .....

### 9.5 User and Role Mappings in Keycloak

Create a user in keycloak and assign default 'service-user' or
'service-admin' role mapping in keycloak.

These roles are default for the client 'service' and you can assign the
role to a user via "Role Mappings" tab.

![](./media/image27.png)

## 10 Custom Metrics Monitoring

Phoenix platform uses Prometheus store data and grafana to view the
metrics in dashboard.

Here are the steps enable metrics endpoint for scraping in Prometheus.

### 10.1 phoenixmsp helm template

To enable to this feature in phoenixmsp templates for your
service,follow the below steps

* In values.yaml , GO to 'serviceMonitor' section remove {} (brackets)
and uncomment the following section
```
endpoints: 
  - port: http 
  interval: 10s 
  path: /msp/hello/metrics
```
Make sure you set the metrics endpoint path is correct or else set the
proper path.

* Go to templates/<SERVICE\>.yaml file and uncomment ServiceMonit.tpl section.

After this deploy using service using 'helm install..' commands.

### 10.2 MSP Framework

Java MSP Framework by default provides endpoint /msp/home/metrics to
publish jersey exporter for Prometheus monitoring. Override the endpoint
in your service and provide your endpoint in values.yaml if you have
custom metrics.

### 10.3 Explanation of ServiceMonitor Object and Prometheus 

The following section is to help understand how Prometheus works and
ServiceMonitor object. You **do not need to do any of the following
steps if use the above section 11.1** to enable Metrics.

*  Make sure 'app' label is setup in service.

Prometheus uses 'app' label in 'service' resource to discover the
application.

Here is an example 'service' resource with app label
```
# Source: helloservice/templates/phoenix.yaml 
apiVersion: v1 
kind: Service 
metadata: 
  name: metricsdemo 
  namespace: dp-dev 
  labels: 
    app: metricsdemo
```
* Deploy 'servicemonitor' resource

Prometheus uses 'servicemonitor' object to find the application and
endpoint to access metrics.

The following is the example configuration of 'servicemetrics'.please
modify this for your needs.

Modify the highlighted section per your configuration.
```
apiVersion: monitoring.coreos.com/v1 
kind: ServiceMonitor 
metadata: 
  name: metricsdemo 
  namespace: dp-dev 
  labels: 
    release: prom 
    app: metricsdemo
spec: 
  endpoints: 
  - port: http 
  interval: 10s 
  path: /msp/hello/metrics 
 namespaceSelector: 
  matchNames: 
  - dp-dev 
 selector: 
  matchLabels: 
    app: metricsdemo
```
* Go to Prometheus and find out your service is listed in targets
section.

Here is link for 'phoenix' cluster

Prometheus: <http://prom.phoenix-admin.landmarksoftware.io/targets>

![](./media/image28.png)

* Grafana dashboard

Here is link for grafana dashboard.

Grafana: http://grafana.phoenix-admin.landmarksoftware.io

Login to grafana dashboard with provided username and password and
create dashboard by access the Prometheus data.

Access the JSON used to create this dashboard in the following git link
for reference.

<https://git.openearth.community/DistServices/core/blob/master/msp-example/com.lgc.dist.core.msp.example.helloservice/monitoring/metrics-dashboard.json>

Here is an example dashboard:

![](./media/image29.png)

### 10.4 Application Billable Metrics

All the billable metrics need to be pushed by the relevant application
services to the billable event manager service (as described below),
which in turn will store them in a highly available, resilient database.
Application is responsible to push billable events when they have
actually happened and customer can be charged for it.

*  Billable event manager microservice will be always running in the
    Kubernetes cluster with service name as billingeventmgr.

    Internal URL:
    [http://billingeventmgr.plat-system/](http://billingeventmgr.plat-system/msp/v2/events)

    External URL:
    <https://CLUSTER_NAME.landmarksoftware.io/services/billingeventmgr/>

    External URL is useful to run postman test during testing but it
    should never be exposed to customers in a production environment.
    Internal URL should be used in code/any configurations for
    inter-service communications.

*  Application services can call the events POST end point on this
    microservice to record an event that should be ultimately billed to
    a client. Here are the Billable event json details that are required
    for creating the event

    ![](./media/image30.png)
    
    ![](./media/image31.png)
    
    Applications need to make sure that appName, eventUuid pair is
    unique for every new event. eventUuid can be any unique identifier
    for the specific appName, there are no format requirements for the
    eventUuid except that it needs to be unique for the associated app.
    Event with same <appName, eventUuid\> fields will be considered
    duplicate and hence won't be recorded. All the events fields are
    mandatory. Response header for this POST request will provide
    content location which could be used to retrieve the recorded event.
```
{ 
    "date": "Wed, 11 Sep 2019 19:51:00 GMT", 
    "server": "istio-envoy",
    "transfer-encoding": "chunked",
    "access-control-allow-methods": "GET, POST, PUT, DELETE, OPTIONS, HEAD",
    "content-type": "text/plain",
    ...
    "content-location": "https://CLUSTER_NAME.landmarksoftware.io:80/services/billingeventmgr/msp/v2/events/testengine/JOB23_EVENT99",
    "access-control-allow-headers": "origin,
    content-type, accept, authorization"
} 
```
Curl command example for posting an event (POST endpoint URL
<http://billingeventmgr.plat-system/msp/v2/events>):
```
curl -X POST --header 'Content-Type: application/json' --header
 'Accept: text/plain' -d '{ 
 \ "appName": "testengine", 
 \ "userEmail": "test%40gmail.com", 
 \ "eventType": "PDS365TEST",
 \ "eventTimestampSecs": 924567899,
 \ "numericEventValue": "1.2",
 \ "eventUuid": "JOB23_EVENT99" \
 }' 'http://billingeventmgr.plat-system/msp/v2/events
```

*  Application services can also call the events GET end point on this
   microservice to query an existing event using appName and eventUuid.
   If the event just got posted, wait for 5 secs or so before querying it.
   Event creation is asynchronous process and when the event gets available
   for querying will depend on network and NewRelic load at that time.

*  On event submission/POST, One needs to ensure that they get 200 response
   code indicating that NewRelic has received the event. One will 
   have to retry event submission until its acknowledged with 200 response code.

*  Asynchronous processing at NewRelic end happens after they have received the event/returned 200
   response code. NewRelic checks syntax of the event/parses data as part of this
   asynchronous processing, If the syntax is valid, event data gets stored. On failure,
   event won't be stored and will be reported in an error table. Failure scenario is possible 
   due to malformed event data which we should be able to detect and resolve in development stages of an application.

### 10.5 Prometheus querying api

To access metrics using http api from a Prometheus server, with query
language expressions

The current stable HTTP API is reachable under /api/v1 on a Prometheus
server.

API response is JSON format with successful request returning 2xx status
code.

Expression queries shall be used.

*  Instant queries - evaluates an instant query at a single point in
    time.
```
GET /api/v1/query
URL query parameters:
 Query=<string>: Prometheus expression query string. 
 time=<rfc3339 | unix_timestamp>: Evaluation timestamp. Optional. If omitted current server time is used.
 timeout=<duration>: Evaluation timeout. Optional. Defaults to and is capped by the value of the -query.timeout flag.
 
 Example: To get the list of nodes in ready status 
 curl -X GET 'http://prom-prometheus-operator-prometheus.monitoring:9090/api/v1/ query=kube_node_status_condition{condition="Ready",status="true"}&time=1568008438' -H 'Authorization: Basic XYZ’
```   
*  Range queries - evaluates an expression query over a range of time.
```
GET /api/v1/query_range 
URL query parameters: 
 query=<string>: Prometheus expression query string. 
 start=<rfc3339 | unix_timestamp>: Start timestamp. 
 end=<rfc3339 | unix_timestamp>: End timestamp. 
 step=<duration>: Query resolution step width. 
 timeout=<duration>: Evaluation timeout. Optional. Defaults to and is capped by the value of the -query.timeout flag. 

Example: To get the list of nodes in ready status in a time range 
curl -X GET 'http://prom-prometheus-operator-prometheus.monitoring:9090/api/v1/ query=kube_node_status_condition{condition="Ready",status="true &start=1568956710&end=1568978340&step=30' -H 'Authorization: Basic XYZ’
```
For more options with http api, Refer <https://prometheus.io/docs/prometheus/2.0/querying/api/>

### 10.6 Monitoring services and infrastructure using NewRelic
Phoenix kubernetes clusters are configured with NewRelic infrastructure agent to provide increased visibility
into the performance of kubernetes environment ie nodes, namespaces, deployments, replica sets, pods and containers.
To find and use data reported from NewRelic kubernetes integration, 
refer <https://docs.newrelic.com/docs/integrations/kubernetes-integration/understand-use-data/understand-use-data>

#### APM agent configuration:

Each container needs to be installed with NewRelic APM agent to monitor container specific performance.
There are 2 parts to this configuration:

1] NewRelic agent to be embedded in the microservice. 
Please refer to newrelic APM agent installation for languages other than Java and Node.
MSP java framework 2.1.0-SNAPSHOT version onwards has been updated to auto configure new relic agent
for msp java microservices. We are in the process of updating msp node framework as well for the same.
Please refer to Java SDK Dev Guide for the details. 

2] NEW_RELIC parameters/env variables to be defined as part of service's helm chart. Phoenixmsp template release 3.3 includes all these settings 
for appropriate newrelic account for the cluster.

NewRelic java agent dependency on MSP java containers: 
<https://git.openearth.community/DistServices/core/blob/master/com.lgc.dist.core.msp.grizzly/pom.xml#L233>

NewRelic java agent configuration in docker script + appropriate concatenation of NewRelic parameters which needs to happen for all languages (not just java) 
<https://git.openearth.community/DistServices/core/blob/master/com.lgc.dist.core.msp.grizzly/src/main/docker/script/start.sh#L25>

NewRelic environment variable configuration on helm templates: 
<https://git.openearth.community/DistServices/helm-deploy/blob/master/phoenixmsp/templates/_helpers.tpl#L247>

#### Non Java/Node services or if not using MSP Java/Node framework:
1] NewRelic agent needs to be embedded in the microservice. Please refer to newrelic APM agent installation for languages other than Java and Node.

2] Configure logs in context to link to your log data with related data across the rest of the New Relic platform. 

3] Perform NewRelic environment variable concatenation as done on above start script to define NEW_RELIC_APP_NAME and update NEW_RELIC_LABELS.

More details on APM agent installations can be found here: <https://docs.newrelic.com/docs/agents>

More details on configuring logs in context can be found here: <https://docs.newrelic.com/docs/logs/enable-logs/configure-logs-context/configure-logs-context-apm-agents>

#### NewRelic secrets
NewRelic account information accessible on the cluster using following secrets, available on all namespaces:

1] newrelic-agent secret with licensekey information to be used for APM agent configuration

2] newrelic-account-details secret with accountid, insertkey and querykey information to be used when pushing/retrieving custom events/metrics from newrelic.

## 11 Override Command in Deployment 

The command and arguments that you define in the configuration file
override the default command and arguments provided by the container
image. If you define args, but do not define a command, the default
command is used with your new arguments. Command is similar to the entry
point in docker. If a command was defined in deployment yaml file, the
script which will be executed at the time of container start will be
replaced with the command defined.
```
apiVersion: apps/v1beta2 
kind: Deployment 
metadata: 
  name: myservice 
  namespace: <your namespace> 
  labels: 
……. 
….. 
… 
spec: 
  replicas: 1 
  selector: 
  matchLabels: 
    app.kubernetes.io/name: myservice1 
  template:
  metadata: 
    labels: 
      app.kubernetes.io/name: myservice1 
  spec: 
    containers: 
    - name: myservice1 
    image: <service_image_name> 
    ports: 
        - name: http 
        containerPort: 8080 
        …………………….. 
        …………………… 
        command: ["/bin/sh", "/javarun/entry.sh"] 
        args: ["testarg"]
```

command: ["/bin/sh", "/javarun/entry.sh"] – entry.sh replaces the default EntryPoint script

args: ["testarg"] - command line arguments for the script entry.sh

When you override the default EntryPoint and Cmd, these rules apply:

-   If you do not supply command or args for a Container, the defaults
    defined in the Docker image are used.

-   If you supply a command but no args for a Container, only the
    supplied command is used. The default EntryPoint and the default Cmd
    defined in the Docker image are ignored.

-   If you supply only args for a Container, the default Entrypoint
    defined in the Docker image is run with the args that you supplied.

-   If you supply a command and args, the default Entrypoint and the
    default Cmd defined in the Docker image are ignored. Your command is
    run with your args.

## 12 Horizontal Pod Autoscaler

To enable scaling of a custom service, the developer needs to create
separate a pod. Here is an example of how to implement it:
```
apiVersion: autoscaling/v2beta1 
kind: HorizontalPodAutoscaler 
metadata: 
  name: hpa-test 
spec: 
  scaleTargetRef: 
    apiVersion: apps/v1 
    kind: Deployment 
    name: helloservice 
  minReplicas: 1 
  maxReplicas: 5 
  metrics: 
  - type: Pods
  pods: 
    metricName: cpu_usage 
    targetAverageValue: 1m
```
## 13 WebSocket Applications

To deploy a web socket application in Phoenix platform, Need to provide
the below value (true) in values.yaml of your websocket application
helmchart.

webSocketUpgrade: true

## 14 InitContainers

InitContainers are specialized containers that run before app containers in a Pod. InitContainers can contain utilities or setup scripts not present in an app image. App containers will start upon the successful start of InitContainer(s).

Incase any of the init containers got failed, app container will never start. Based on ‘RestartPolicy’ value, kubernetes will try to restart pod until InitContainer(s) succeed.

To add InitContainer(s) to the deployment, provide required values in values.yaml of your helm chart as below

```
# To provide initcontainers setup, remove {} and uncomment and modify the following configuration as per your init container requirements. There could be 0..n initcontainer configs.

# Following parameters are configured for initilizing postgres db by executing createDB script, it depends on postgres related env variables and CREATEDB variable specifying the db name.

# This configuration is just an example though, one can configure any other docker image with init script to perform the required initialization before the service container starts up.
initcontainers:
  - name: initdb
    ports:
      - name: http
        containerPort: 80
        protocol: TCP
    image:
      repository: distplat-docker-milestone.hub.ienergycloud.io/distarch/com.lgc.dist.core.msp.init.scripts
      tag: "0.1-SNAPSHOT"
      pullPolicy: Always
    command: "[\"sh\", \"/scripts/createDB.sh\"]"
    volumeMounts: 
      - name: transient-data
        mountPath: /tmp

```

## 15 TransientVolumes

It is used to support for emptyDir volume type, So that Volume can be shared to multiple container for communication in a pod.
Created when a Pod is assigned to a Node and exists as long as that Pod is running on that node.

To add transientvolumes to the deployment, provide required values in values.yaml of your helm chart as below

```
#To create transient or emptyDir volume for pod life cycle, provide name, mount path, sizeLimit and medium as required for primary container.
transientvolumes: 
  - name: transient-data
    mountPath: /tmp
    sizeLimit: 10Mi
  - name: transient-data1
    mountPath: /tmp1
    medium: Memory
    sizeLimit: 10Mi

```


